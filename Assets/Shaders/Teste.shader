﻿Shader "Introduction/Teste"
{
    Properties
    {

        _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}

        [Enum(UnityEngine.Rendering.BlendMode)]
        _SourceFactor("Source Factor", Float) = 5
        [Enum(UnityEngine.Rendering.BlendMode)]
        _DestinationFactor("Destination Factor", Float) = 10
        [Enum(UnityEngine.Rendering.BlendOp)]
        _Opperation("Operation", Float) = 0
    }
    SubShader
    {
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        ZWrite Off
        Lighting Off
        Cull Off
        Fog { Mode Off } 
        LOD 110
        Blend [_SourceFactor] [_DestinationFactor]
        BlendOp [_Opperation]
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert_vct
            #pragma fragment frag_mult
            #pragma fragmentoption ARB_precision_hint_fastest
            #include "UnityCG.cginc"
            sampler2D _MainTex;
            float4 _MainTex_ST;

            struct vin_vct
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 texcoord : TEXCOORD0;
            };

             struct v2f_vct
             {
                float4 vertex : POSITION;
                fixed4 color : COLOR;
                half2 texcoord : TEXCOORD0;
             };

            sampler2D _MainTexture;
            float4 _MainTexture_ST;

            float4 _Color;

            v2f_vct vert_vct(vin_vct v)
            {
                v2f_vct o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = v.color;
                o.texcoord = v.texcoord;
                return o;
            }
            
            float4 frag_mult(v2f_vct i) : COLOR
            {
                float4 tex = tex2D(_MainTex, i.texcoord);
                return tex;
            }
            ENDCG
        }
    }
}