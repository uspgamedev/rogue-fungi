﻿Shader "Custom/Glint"
{
    Properties
    {
        _MainTex ("Particle Texture", 2D) = "white" {}
        _TintColor ("Tint Color", Color) = (1, 1, 1, 1)
        _GlintColor ("Glint Color", Color) = (1, 1, 1, 1)
        _GlintWidth ("Glint Width", Range(0.0, 1.0)) = 0.2
        _GlintSoftness ("Glint Softness", Range(0.0, 1.0)) = 0.5
        _GlintDuration ("Glint Duration", Range(0.1, 10.0)) = 1.0
        _GlintAngle ("Glint Angle", Range(0.0, 360.0)) = 45.0 // Angle in degrees
        _GlintDelay ("Glint Delay", Range(0.0, 10.0)) = 0.0
    }
    SubShader
    {
        Tags { "Queue"="Overlay" }
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off
        Cull Off
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                float4 color : COLOR; // Particle color
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 color : COLOR; // Particle color
            };

            sampler2D _MainTex;
            float4 _TintColor;
            float4 _GlintColor;
            float _GlintWidth;
            float _GlintSoftness;
            float _GlintDuration;
            float _GlintAngle;
            float _GlintDelay;

            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;
                o.color = v.color; // Pass particle color to fragment shader
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // Tempo global para o efeito do glint com atraso
                float time = _Time.y;
                float glintProgress = saturate((time - _GlintDelay) / _GlintDuration);

                // Convert angle to radians and calculate direction
                float angle = _GlintAngle * 3.14159265359 / 180.0;
                float2 direction = float2(cos(angle), sin(angle));

                // Calculate offset in the direction of the glint
                float offset = dot(i.uv - 0.5, direction); // Centering the effect

                // Determine the start and end positions of the effect
                float glintStart = -0.5 - _GlintWidth - _GlintSoftness;
                float glintEnd = 0.5 + _GlintWidth + _GlintSoftness;
                float glintPos = lerp(glintStart, glintEnd, glintProgress);

                // Calculate the effect of the glint considering width and softness
                float glintStartEffect = glintPos - _GlintWidth - _GlintSoftness;
                float glintEndEffect = glintPos + _GlintWidth + _GlintSoftness;
                float glintEffect = smoothstep(glintStartEffect, glintPos - _GlintWidth, offset) - 
                                    smoothstep(glintPos + _GlintWidth, glintEndEffect, offset);

                // Get the base texture color
                fixed4 texColor = tex2D(_MainTex, i.uv);

                // Apply the glint effect without affecting alpha
                fixed4 glintColor = _GlintColor * glintEffect;
                glintColor.a = texColor.a; // Preserve original alpha

                // Multiply by the tint color (affects alpha and color over lifetime)
                fixed4 finalColor = texColor * _TintColor + glintColor;
                finalColor *= i.color;

                // Ensure final color alpha is not greater than 1.0
                finalColor.a = min(finalColor.a, 1.0);

                return finalColor;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
