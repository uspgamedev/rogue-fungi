﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Localization.Components;

public class ActionSlotBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public BattleUIManager battleUIManager;
	public Animator animator;
	public Image thisImage;
	public Text thisText;
	public int action;
	public Color slotTextColorGray = new Color(0.19f, 0.19f, 0.19f);

	public GameObject textExplainerObject;
	public LocalizeStringEvent localizedString;

	public void OnPointerEnter(PointerEventData pointerEventData)
	{
		textExplainerObject.SetActive(true);
		localizedString.StringReference.SetReference("EnemyActionExplainer", gameObject.name);
	}

	public void OnPointerExit(PointerEventData pointerEventData)
	{
		textExplainerObject.SetActive(false);
	}

	public void HideSlot()
	{
		this.gameObject.SetActive(false);
	}

	public void ShowSlot()
	{
		this.gameObject.SetActive(true);
	}

	public void Activate()
	{
		animator.SetTrigger("Activate");
	}

	public void ChangeToAttack(int damageNumber)
	{
		gameObject.name = "enemyAttack";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotAttack;
		thisImage.color = Color.white;
		thisText.text = damageNumber.ToString();
		thisText.color = slotTextColorGray;
	}

	public void ChangeToProtect(int defenseNumber)
	{
		gameObject.name = "enemyProtect";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotProtect;
		thisImage.color = Color.white;
		thisText.text = defenseNumber.ToString();
		thisText.color = Color.white;
	}

	public void ChangeToPoison(int poisonNumber)
	{
		gameObject.name = "enemyPoison";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotPoison;
		thisImage.color = Color.white;
		thisText.text = poisonNumber.ToString();
		thisText.color = slotTextColorGray;
	}

	public void ChangeToHeal(int healNumber)
	{
		gameObject.name = "enemyHeal";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.genericSprite;
		thisImage.color = Color.white;
		thisText.text = healNumber.ToString();
		thisText.color = slotTextColorGray;
	}

	public void ChangeToClumsiness(int turns)
	{
		gameObject.name = "enemyClumsiness";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotClumsiness;
		thisImage.color = Color.white;
		thisText.text = turns.ToString();
		thisText.color = slotTextColorGray;
	}

	public void ChangeToNastyPresent()
	{
		gameObject.name = "enemyNastyPresent";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotNastyPresent;
		thisImage.color = Color.white;
		thisText.text = "";
		thisText.color = slotTextColorGray;
	}

	public void ChangeToGainStrength(int strength)
	{
		gameObject.name = "enemyGainStrength";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotGainStrength;
		thisImage.color = Color.white;
		thisText.text = strength.ToString();
		thisText.color = slotTextColorGray;
	}

	public void ChangeToTiredness(int ammount)
	{
		gameObject.name = "enemyTiredness";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotFatigue;
		thisImage.color = Color.white;
		thisText.text = ammount.ToString();
		thisText.color = slotTextColorGray;
	}

	public void ChangeToSlotBlock(string slot)
	{
		gameObject.name = "enemySlotBlock";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotBlocked;
		thisImage.color = Color.white;
		thisText.text = slot;
		thisText.color = slotTextColorGray;
	}

	public void ChangeToSpecialProtect(int specialProtection)
	{
		gameObject.name = "enemySpecialProtect";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotSpecialProtection;
		thisImage.color = Color.white;
		thisText.text = specialProtection.ToString();
		thisText.color = slotTextColorGray;
	}

	public void ChangeToCurse(int turns)
	{
		gameObject.name = "enemyCurse";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotCurse;
		thisImage.color = Color.white;
		thisText.text = turns.ToString();
		thisText.color = Color.white;
	}

	public void ChangeToDestruction(int turns)
	{
		gameObject.name = "enemyDestruction";
		animator.SetTrigger("FadeIn");
		thisImage.sprite = battleUIManager.actionSlotProtect;
		thisImage.color = Color.white;
		thisText.text = turns.ToString();
		thisText.color = Color.white;
	}
}
