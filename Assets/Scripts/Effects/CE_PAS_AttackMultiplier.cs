﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_PAS_AttackMultiplier : CardsEffects
{
	public int[] damageMultiplierPerLevel;
	public int actualMultiplier;
	public int index = 2;

	public void Start()
	{
		actualMultiplier = damageMultiplierPerLevel[thisCardStatus.level];
		index = thisCardStatus.level;
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameManager.Instance.battleManager.damageMultiplierNextAttack = damageMultiplierPerLevel[thisCardStatus.level];
		battleUIManagerScript.ShowPlayerDamageMultiplier();
	}

	public override void Upgrade(int level, bool highlight)
	{
		index = level;
		actualMultiplier = damageMultiplierPerLevel[level];
		RefreshCardStrings();
	}

}
