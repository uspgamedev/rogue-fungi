﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_HAB_LeafTea : CardsEffects
{
	public int[] healingPerLevel;
	public int actualHealing;

	public void Start()
	{
		actualHealing = healingPerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		PlayerBehaviour.Instance.HealLife(actualHealing);
	}

	public override string CardDescription()
	{
		return string.Format(effectText, GameUIManager.Instance.cardDescriptionColor);
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualHealing = healingPerLevel[level];
		RefreshCardStrings();
	}
}
