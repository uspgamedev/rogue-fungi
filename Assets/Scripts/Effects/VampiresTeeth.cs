﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VampiresTeeth : CardsEffects
{
	public int damage;

	public override void ApplyEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(damage);
	}

	public override string CardDescription()
	{
		return string.Format(effectText, damage);
	}
}
