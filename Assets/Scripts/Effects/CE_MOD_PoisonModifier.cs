﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.SmartFormat.PersistentVariables;

public class CE_MOD_PoisonModifier : CardsEffects
{
	public int[] poisonPerLevel;
	public int actualPoison;

	void Start()
	{
		actualPoison = poisonPerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameObject slot = GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn];
		SlotBehaviour thisSlotBehaviour = slot.GetComponent<SlotBehaviour>();

		thisSlotBehaviour.actualModifier = CardModifier.POISON;
		thisSlotBehaviour.slotImage.color = battleUIManagerScript.color_PoisonSlot;
		thisSlotBehaviour.modifierImage.SetActive(true);
		thisSlotBehaviour.modifierImage.GetComponent<Image>().sprite = battleUIManagerScript.sprite_PoisonSlot;
		thisSlotBehaviour.modifierImage.GetComponent<Outline>().effectColor = battleUIManagerScript.color_PiercingSlot;
		thisSlotBehaviour.powerSlot = actualPoison;
		thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Poison");
		thisSlotBehaviour.localizedString.StringReference.Add("v", new ObjectVariable {Value = thisSlotBehaviour});
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualPoison = poisonPerLevel[level];
		RefreshCardStrings();
	}

	public override string CardDescription()
	{
		return string.Format(effectText, poisonPerLevel[thisCardStatus.level]);
	}
}
