﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_ATQ_BraveDamage : CardsEffects
{
	public int actualDamage;
	public int[] braveDamagePerLevel;
	public int actualBraveDamage;

	public void Start()
	{
		actualBraveDamage = braveDamagePerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualBraveDamage);
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualBraveDamage = braveDamagePerLevel[level];
		RefreshCardStrings();
	}

	public override void ApplyPoisonModifierEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage);
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyProtectionEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage);
		player.GetComponent<PlayerBehaviour>().playerBehaviourData.defenseCurrent += GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot;
	}

	public override void ApplyPiercingEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage);
	}

	public override void ApplyStrengthEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override string CardDescription()
	{
		return string.Format(effectText, actualDamage);
	}
}
