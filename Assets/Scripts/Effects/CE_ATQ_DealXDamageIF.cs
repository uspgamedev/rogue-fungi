using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_ATQ_DealXDamageIF : CardsEffects
{
	public int[] damagePerLevel;
	public int[] otherDamagePerLevel;
	public int actualDamage;
	public int otherDamage;

	public void Start()
	{
		actualDamage = damagePerLevel[thisCardStatus.level];
		otherDamage = otherDamagePerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}
	private List<int> getIndexOtherCardsOnTable()
	{
		List<int> choices = new List<int>(new int[] {0, 1, 2});
		choices.Remove(thisCardStatus.slotIsOn);
		return choices;
	}

	public override void ApplyEffect()
	{
		List<int> choices = getIndexOtherCardsOnTable();
		KindOfCard type1 = DeckBehaviour.Instance.onTable[choices[0]].GetComponent<CardStatus>().kind;
		KindOfCard type2 = DeckBehaviour.Instance.onTable[choices[1]].GetComponent<CardStatus>().kind;

		if(type1 == KindOfCard.ATTACK && type2 == KindOfCard.ATTACK)
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(otherDamage);
		else
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage);
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualDamage = damagePerLevel[level];
		otherDamage = otherDamagePerLevel[level];
		RefreshCardStrings();
	}
	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyProtectionEffect()
	{
		ApplyEffect();
		player.GetComponent<PlayerBehaviour>().playerBehaviourData.defenseCurrent +=  GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot;
	}

	public override void ApplyPiercingEffect()
	{
		List<int> choices = getIndexOtherCardsOnTable();
		KindOfCard type1 = DeckBehaviour.Instance.onTable[choices[0]].GetComponent<CardStatus>().kind;
		KindOfCard type2 = DeckBehaviour.Instance.onTable[choices[1]].GetComponent<CardStatus>().kind;

		if(type1 == KindOfCard.ATTACK && type2 == KindOfCard.ATTACK)
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(otherDamage);
		else
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(actualDamage);

		actualDamage = damagePerLevel[thisCardStatus.level];
		otherDamage = otherDamagePerLevel[thisCardStatus.level];
	}

	public override void ApplyStrengthEffect()
	{
		List<int> choices = getIndexOtherCardsOnTable();
		KindOfCard type1 = DeckBehaviour.Instance.onTable[choices[0]].GetComponent<CardStatus>().kind;
		KindOfCard type2 = DeckBehaviour.Instance.onTable[choices[1]].GetComponent<CardStatus>().kind;

		if(type1 == KindOfCard.ATTACK && type2 == KindOfCard.ATTACK)
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(otherDamage + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
		else
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);

		actualDamage = damagePerLevel[thisCardStatus.level];
		otherDamage = otherDamagePerLevel[thisCardStatus.level]; 
	}

	public override string CardDescription()
	{
		return string.Format(effectText, actualDamage);
	}
}
