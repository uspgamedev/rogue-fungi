﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_PAS_Anger : CardsEffects
{
	public int[] addDamagePerLevel; // dano acrescido a cada turno
	public CE_ATQ_DealDamage ce_atq_dealDamage;
	public int actualAddDamage;

	public void Start()
	{
		hasPassiveEffect = true;
		actualAddDamage = addDamagePerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyPassiveEffect()
	{
		ce_atq_dealDamage.actualDamage += actualAddDamage;
		ce_atq_dealDamage.dhos[0] = true;
		RefreshCardStrings();
	}
	public override void Upgrade(int level, bool highlight)
	{
		actualAddDamage = addDamagePerLevel[level];
		RefreshCardStrings();
	}

	public void OnDisable()
	{
		ce_atq_dealDamage.dhos[0] = false;
	}
}
