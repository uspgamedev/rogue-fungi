﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;

public class CE_HAB_AddDefense : CardsEffects
{
	public bool ressurectionEffect;
	public int[] defensePerLevel;
	public int[] defenseBoostPerLevel;
	public int actualDefense;
	public int actualDefenseBoost;

	public void Start()
	{
		actualDefense = defensePerLevel[thisCardStatus.level];
		actualDefenseBoost = defenseBoostPerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		PlayerBehaviour.Instance.playerBehaviourData.defenseCurrent += actualDefense;
		if (ressurectionEffect)
		{
			dhos[0] = true;
			actualDefense += defenseBoostPerLevel[thisCardStatus.level];
		}
		RefreshCardStrings();
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualDefense = defensePerLevel[level];
		actualDefenseBoost = defenseBoostPerLevel[level];
		RefreshCardStrings();
	}

	public override string CardDescription()
	{
		return string.Format(effectText, defensePerLevel[thisCardStatus.level]);
	}
}
