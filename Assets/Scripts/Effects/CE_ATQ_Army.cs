﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_ATQ_Army : CardsEffects
{
	public CardUnit cardUnit;
	public int[] troopDamagePerLevel;
	public int actualTroopDamage;
	public int actualTroopCount;

	void Start()
	{
		actualTroopDamage = troopDamagePerLevel[thisCardStatus.level];
		actualTroopCount = DeckBehaviour.Instance.troopCount;
		RefreshCardStrings();
	}

	private void CreateNewTroop()
	{
		GameObject newTroop = Instantiate(cardUnit.cardPrefab, battleUIManagerScript.battleUI.transform, false);
		DeckBehaviour.Instance.AddTemporaryCardToDeckList(newTroop);
		newTroop.GetComponent<IDragger>().SendCardOffScreen();
		battleUIManagerScript.ShowDeckNumberCards();
		battleUIManagerScript.ShowGraveyardNumberCards();
		DeckBehaviour.Instance.troopCount ++;
		actualTroopCount = DeckBehaviour.Instance.troopCount;
	}

	public override void ApplyEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualTroopCount * troopDamagePerLevel[thisCardStatus.level]);
		CreateNewTroop();
		RefreshCardStrings();
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualTroopDamage = troopDamagePerLevel[level];
		RefreshCardStrings();
	}

	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyProtectionEffect()
	{
		ApplyEffect();
		player.GetComponent<PlayerBehaviour>().playerBehaviourData.defenseCurrent += GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot;
	}

	public override void ApplyPiercingEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(actualTroopCount * troopDamagePerLevel[thisCardStatus.level]);
		CreateNewTroop();
		RefreshCardStrings();
	}

	public override void ApplyStrengthEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualTroopCount * troopDamagePerLevel[thisCardStatus.level] + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
		CreateNewTroop();
		RefreshCardStrings();
	}

	public override string CardDescription()
	{
		return string.Format(effectText, actualTroopCount * troopDamagePerLevel[thisCardStatus.level]);
	}
}
