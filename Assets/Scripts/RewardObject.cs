﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static UnityEngine.ParticleSystem;
using TMPro;

public class RewardObject : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
	[Header("Setup")]
	public RewardSelector rewardSelector;
	public RectTransform rectTransform;
	public SpriteRenderer essenceImage;
	public SpriteRenderer overlayImage;
	public Animator animator;
	public ParticleSystem particles;
	[Header("Current Reward")]
	public int level;
	[HideInInspector] public bool isSelected;
	[HideInInspector] public string cardDescription;

	public bool clickDown = false;
	public bool interactable = false;

	public Vector3 originalPosition;

	public GameObject card;
	public GameObject cardPrefab;

	public void Awake()
	{
		if (RewardData.rewardType == 1)
		{
			SetNewOriginalPos();
			interactable = true;
		}
	}
		public void SetupCardEssence(GameObject instantiatedCard, GameObject selectedCardPrefab)
	{
		cardPrefab = selectedCardPrefab;
		card = instantiatedCard;
		card.transform.position = new Vector3(10000.0f, 10000.0f, 10000.0f);
		card.GetComponent<CardsEffects>().RefreshCardStrings();
		essenceImage.sprite = card.GetComponent<CardStatus>().cardCenterSprite;
		overlayImage.sprite = essenceImage.sprite;
	}

	public string CardDescription()
	{
		cardDescription = "";
		Transform descriptions = card.transform.Find("Description");
		foreach (Transform description in descriptions)	cardDescription += description.GetComponent<TextMeshPro>().text + '\n';
		return cardDescription;
	}

	public void OnPointerDown(PointerEventData pointerEventData)
	{
		clickDown = true;
	}

	public void OnPointerUp(PointerEventData pointerEventData)
	{
		if (clickDown && interactable)	rewardSelector.SetRewardOnBook(pointerEventData);
		else	ReturnObject(gameObject);

		clickDown = false;
	}

	public void ReturnObject(GameObject obj)
	{
		rewardSelector.SnapBack(obj);
		if (rewardSelector.selectedReward == null)	rewardSelector.SetSelectionElements(false);
		else if (rewardSelector.selectedReward != obj)	rewardSelector.SetSelectionElements(true);
	}

	public void OnBeginDrag(PointerEventData pointerEventData)
	{
		clickDown = false;
		if(animator.gameObject.activeSelf && animator.runtimeAnimatorController!=null) animator.SetBool("IsDragging", true);
		rewardSelector.SetSelectionElements(false);
		if(rewardSelector.selectedReward == gameObject)	rewardSelector.selectedReward = null;
	}

	// Arrasta a carta para a posicao do dedo
	public void OnDrag(PointerEventData pointerEventData)
	{
		rectTransform.anchoredPosition += pointerEventData.delta / rewardSelector.rewardCanvas.scaleFactor;
	}

	public void OnEndDrag(PointerEventData pointerEventData)
	{
		if(animator.gameObject.activeSelf && animator.runtimeAnimatorController!=null) animator.SetBool("IsDragging", false);
		Logging.Log("EndDrag: " + isSelected);
		if (!isSelected)	ReturnObject(gameObject);
	}

	public void OnDisable()
	{
		isSelected = false;
		cardDescription = null;
	}
    public void SetNewOriginalPos()
    {
		originalPosition = rectTransform.position;
    }
	public void FinishedAnimation()
	{
		interactable = true;
	}
}
