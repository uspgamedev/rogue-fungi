﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Programa que cuida do comportamento do inimigo*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using CardCopy;
using UnityEngine.UI;

public class EnemyBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public BattleUIManager battleUIManagerScript;
	public GameObject player;

	public EnemyUnit thisEnemyUnit;
	public int specialProtectionTurns;
	public GameObject cardAttacking;
	public CardUnit cardUnitAttacking;

	public DeckBehaviour playerDeck;

	public HealthBar enemyHealthBar;

	public Sprite defaultSprite;
	public Sprite highlightSprite;

	private EnemyDisplay enemyDisplay;

	void Start()
	{
		battleUIManagerScript = GameObject.FindGameObjectWithTag("BattleUIManager").GetComponent<BattleUIManager>();
		enemyDisplay = this.gameObject.GetComponent<EnemyDisplay>();
		playerDeck = DeckBehaviour.Instance;
		player = GameObject.FindGameObjectWithTag("Player");
		enemyHealthBar = battleUIManagerScript.enemyHealthBar;
		enemyHealthBar.SetMaxHealthValue(thisEnemyUnit.enemyUnitData.originalHealth);
		enemyHealthBar.SetHealth(thisEnemyUnit.enemyUnitData.actualHealth);
	}

	public void OnPointerEnter(PointerEventData pointerEventData)
	{
		if (GameManager.Instance.battleManager.state != BattleState.PLAYERTURN) return;
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        return;
    }

    public IEnumerator EnemyToStealPlayerAction()
    {
        yield return new WaitForSeconds(0.3f);

        if (GameManager.Instance.battleManager.precautionsTaken == 0)
        {
            // colocar aqui o comportamento
        }
        else     GameManager.Instance.battleManager.precautionsTaken--;

    }

    public IEnumerator EnemyToProtection(int protection)
    {
        yield return new WaitForSeconds(0.2f);

        thisEnemyUnit.enemyUnitData.actualProtection += protection;

        yield return new WaitForSeconds(0.3f);

        battleUIManagerScript.ShowEnemyProtection();

    }

    public IEnumerator EnemyToSpecialProtection(int specialProtection, int turns)
    {
        yield return new WaitForSeconds(0.2f);

        Debug.Log("Behaviour special protection: " + specialProtection);
        thisEnemyUnit.enemyUnitData.actualSpecialProtection = specialProtection;
        specialProtectionTurns = turns;

        yield return new WaitForSeconds(0.3f);

        battleUIManagerScript.ShowEnemySpecialProtection();

    }

    public void TakeDamage(int damage)
    {
        int damageBonus = GameManager.Instance.battleManager.bonusDamage;
        int totalDamage = (damage + damageBonus) * GameManager.Instance.battleManager.damageMultiplierNextAttack;
        int damageToTake = totalDamage - thisEnemyUnit.enemyUnitData.actualProtection;

        if (thisEnemyUnit.enemyUnitData.actualProtection > 0)
        {
            thisEnemyUnit.enemyUnitData.actualProtection -= totalDamage;
            if (thisEnemyUnit.enemyUnitData.actualProtection < 0)   thisEnemyUnit.enemyUnitData.actualProtection = 0;
            battleUIManagerScript.ShowEnemyProtection();
        }

        if (damageToTake < 0)
            damageToTake = 0;

        if (thisEnemyUnit.enemyUnitData.actualSpecialProtection > 0 && thisEnemyUnit.enemyUnitData.actualSpecialProtection < damageToTake)
            damageToTake = thisEnemyUnit.enemyUnitData.actualSpecialProtection;

        thisEnemyUnit.enemyUnitData.actualHealth -= damageToTake;

        if (thisEnemyUnit.enemyUnitData.actualHealth < 0)
            thisEnemyUnit.enemyUnitData.actualHealth = 0;

        GameManager.Instance.battleManager.damageMultiplierNextAttack = 1;
        battleUIManagerScript.ShowPlayerDamageMultiplier();

        enemyHealthBar.SetHealth(thisEnemyUnit.enemyUnitData.actualHealth);

        CheckIfNeedToChangeEnemyStage();

        if (thisEnemyUnit.enemyUnitData.actualHealth > 0)
            StartCoroutine(enemyDisplay.VisualTakingDamage(damageToTake));
        else
			StartCoroutine(enemyDisplay.VisualFleeFromBattle());
    }

    public void TakePiercingDamage(int damage)
    {
        int damageBonus = GameManager.Instance.battleManager.bonusDamage;
        int damageToTake = (damage + damageBonus) * GameManager.Instance.battleManager.damageMultiplierNextAttack;

        if (thisEnemyUnit.enemyUnitData.actualSpecialProtection > 0 && thisEnemyUnit.enemyUnitData.actualSpecialProtection < damageToTake)
            damageToTake = thisEnemyUnit.enemyUnitData.actualSpecialProtection;

        thisEnemyUnit.enemyUnitData.actualHealth -= damageToTake;

        GameManager.Instance.battleManager.damageMultiplierNextAttack = 1;
        battleUIManagerScript.ShowPlayerDamageMultiplier();

        enemyHealthBar.SetHealth(thisEnemyUnit.enemyUnitData.actualHealth);

        CheckIfNeedToChangeEnemyStage();

        if (thisEnemyUnit.enemyUnitData.actualHealth > 0)
            StartCoroutine(enemyDisplay.VisualTakingDamage(damageToTake));
        else
			StartCoroutine(enemyDisplay.VisualFleeFromBattle());
    }

    public void TakePoisonDamage()
    {
        thisEnemyUnit.enemyUnitData.actualHealth -= thisEnemyUnit.enemyUnitData.actualPoison;
        battleUIManagerScript.enemyHealthBar.SetHealth(thisEnemyUnit.enemyUnitData.actualHealth);
        battleUIManagerScript.ShowEnemyStatus();

        StartCoroutine(enemyDisplay.VisualTakingPoisonDamage(thisEnemyUnit.enemyUnitData.actualPoison));
		battleUIManagerScript.poisonEnemyParticles.Clear();
		battleUIManagerScript.poisonEnemyParticles.Play();

        CheckIfNeedToChangeEnemyStage();

        if (thisEnemyUnit.enemyUnitData.actualHealth <= 0)
        {
			StartCoroutine(enemyDisplay.VisualFleeFromBattle());
        }
    }

    public void TakePoison(int poison)
    {
        thisEnemyUnit.enemyUnitData.actualPoison += poison;
        enemyDisplay.ChangeColorToPoison();
    }

    public void CheckIfNeedToChangeEnemyStage()
    {
        if (GameManager.Instance.battleManager.objectReferences.enemyActionPatternScript.enemyActionPatternData.enemyStage <
			GameManager.Instance.battleManager.objectReferences.enemyActionPatternScript.enemyActionPatternData.enemyMaxStage)
        {
            if (thisEnemyUnit.enemyUnitData.actualHealth <= (thisEnemyUnit.enemyUnitData.originalHealth / 10) * 4)
                GameManager.Instance.battleManager.EnemyAdvanceStage();
            else if (thisEnemyUnit.enemyUnitData.actualHealth <= (thisEnemyUnit.enemyUnitData.originalHealth / 10) * 7)
                GameManager.Instance.battleManager.EnemyAdvanceStage();
        }
        
    }

}
