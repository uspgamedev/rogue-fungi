﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameUIManager : MonoBehaviour
{
	public static GameUIManager Instance { get; private set; }

	[Header("Systems")]
	public MapBehaviour mapBehaviourScript;
	public RewardManager rewardManagerScript;

	[Header("Screens")]
	public GameObject initialScreen;
	public GameObject chestScreen;
	public GameObject rewardSreen;
	public GameObject battleScreen;
	public GameObject shopScreen;
	public GameObject mapScreen;
	public GameObject upgradeScreen;
	public GameObject restScreen;
	public GameObject settingsMenu;

	[Header("Colors")]
	public string colorBlack;
	public string colorWhite;
	public string colorRed;
	public string colorGreen;
	public string cardDescriptionColor;
	public string cardCostColor;

	[Header("Warnings")]
	public string notEnoughActionPoints;
	public string notEnoughCoins;
	public string noCardsToUpgrade;

	public void Awake()
	{
		if (Instance != null && Instance != this)
			Destroy(gameObject);
		else
			Instance = this;
		DontDestroyOnLoad(gameObject);

		colorBlack = "#000000";
		colorWhite = "#FFFFFF";
		colorRed = "#FF0000";
		colorGreen = "#00FF00";
		cardDescriptionColor = "#F0DCBB";
		cardCostColor = "#3C1B03";
		notEnoughActionPoints = "Sem pontos de ação o suficiente";
		notEnoughCoins = "Sem moedas o suficiente";
		noCardsToUpgrade = "Não nenhuma carta para encantar";
	}

	int InitialScreenToMapAfterTransition()
	{
		GameManager.Instance.state = GameState.MAP_SCREEN;
		return 0;
	}
	int MapToInitialScreenAfterTransition()
	{
		GameManager.Instance.SaveGame();
		GameManager.Instance.state = GameState.INITIAL_SCREEN;

		GameManager.Instance.SaveSettingsData();
		RestartClassesOnReset();

		SceneManager.LoadScene("MenuScene");
		return 0;
	}
	int ShopScreenToMapScreenAfterTransition()
	{
		GameManager.Instance.state = GameState.MAP_SCREEN;
		SceneManager.LoadScene("MapScene");
		GameManager.Instance.transitionObjectAnimator.SetBool("FullAnimation", true);
		return 0;
	}

	int ChestToMapAfterTransition()
	{
		MapSceneTransition.mapSceneTransitionData.needToEnableNextPlace = true;

		GameManager.Instance.state = GameState.MAP_SCREEN;
		GameManager.Instance.SaveGame();

		SceneManager.LoadScene("MapScene");

		return 0;
	}

	int RewardToMapAfterTransition()
	{

		MapSceneTransition.mapSceneTransitionData.needToEnableNextPlace = true;

		GameManager.Instance.state = GameState.MAP_SCREEN;
		GameManager.Instance.SaveGame();

		SceneManager.LoadScene("MapScene");

		return 0;
	}

	int UpgradeToMapAfterTransition()
	{ 
		MapSceneTransition.mapSceneTransitionData.needToEnableNextPlace = true;

		GameManager.Instance.state = GameState.MAP_SCREEN;
		GameManager.Instance.SaveGame();

		SceneManager.LoadScene("MapScene");

		return 0;
	}

	int RestScreenToMapScreenAfterTransition()
	{ 
		MapSceneTransition.mapSceneTransitionData.needToEnableNextPlace = true;

		GameManager.Instance.state = GameState.MAP_SCREEN;
		GameManager.Instance.SaveGame();

		SceneManager.LoadScene("MapScene");

		return 0;
	}

	int EndScreenToMainMenuAfterTransition()
	{
		GameManager.Instance.state = GameState.INITIAL_SCREEN;
		SceneManager.LoadScene("MenuScene");
		return 0;
	}

	void RestartClassesOnReset()
	{
		GameObject.Destroy(DeckBehaviour.Instance.gameObject);
		GameObject.Destroy(PlayerBehaviour.Instance.gameObject);
		MapSceneTransition.ResetClass();
	}

	public void FromShopScreenToMapScreen()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, ShopScreenToMapScreenAfterTransition));
	}
	public void FromInitialScreenToMapScreen()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, InitialScreenToMapAfterTransition));
	}

	public void FromMapToInitialScreenScreen()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, MapToInitialScreenAfterTransition));
	}


	public void FromChestScreenToMapScreen()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, ChestToMapAfterTransition));
	}

	public void FromRewardScreenToMapScreen()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, RewardToMapAfterTransition));
	}

	public void FromUpgradeScreenToMapScreen()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, UpgradeToMapAfterTransition));
	}

	public void FromRestScreenToMapScreen()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, RestScreenToMapScreenAfterTransition));
	}

	public void FromEndScreenToMainMenu()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, EndScreenToMainMenuAfterTransition));
	}

	public void ToggleSettingsMenu()
	{
		bool option = settingsMenu.activeInHierarchy;
		settingsMenu.SetActive(!option);

		switch (GameManager.Instance.state)
		{
			case GameState.INITIAL_SCREEN:
				initialScreen.SetActive(option);
				break;

			case GameState.BATTLE_SCREEN:
				battleScreen.SetActive(option);
				break;

			case GameState.MAP_SCREEN:
				mapScreen.SetActive(option);
				break;

			case GameState.REWARD_SCREEN:
				rewardSreen.SetActive(option);
				break;

			case GameState.CHEST_SCREEN:
				chestScreen.SetActive(option);
				break;

			case GameState.UPGRADE_SCREEN:
				upgradeScreen.SetActive(option);
				break;

			case GameState.SHOP_SCREEN:
				shopScreen.SetActive(option);
				break;

			case GameState.REST_SCREEN:
				restScreen.SetActive(option);
				break;
		}
	}
}
