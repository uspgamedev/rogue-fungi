﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Contem o comportamento do Slot das cartas do jogador*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Localization.Components;
using DG.Tweening;

public enum SlotPosition { LEFT, CENTER, RIGHT };

public class SlotBehaviour : MonoBehaviour, IDropHandler
{

	[Header("System Components")]
	public BattleUIManager battleUIManagerScript;

	[Header("Others")]
	public SlotPosition slot;
	public CardModifier actualModifier = CardModifier.ORIGINAL;
	public int powerSlot;
	public LocalizeStringEvent localizedString;
	public Image slotImage;
	public GameObject modifierImage;

	public void OnDrop(PointerEventData pointerEventData)
	{
		if (!(GameManager.Instance.state == GameState.BATTLE_SCREEN
				&& GameManager.Instance.battleManager.state == BattleState.PLAYERTURN))
			return;

		BattleManager battleManager = GameManager.Instance.battleManager;
		if (battleManager.slotBlocked == (int) slot)
			return;

		GameObject cardBeingDropped = pointerEventData.pointerDrag.gameObject;

		if (cardBeingDropped.tag == "Card")
		{
			CardStatus cardBeingDroppedStatus = cardBeingDropped.GetComponent<CardStatus>();
			CardStatus thisCardStatus = DeckBehaviour.Instance.onTable[(int) slot].GetComponent<CardStatus>();

			Vector3 cardBeingDroppedPosition = GameManager.Instance.battleManager.objectReferences.slots[cardBeingDroppedStatus.slotIsOn].transform.position;
			Vector3 thisCardPosition = GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].transform.position;

			thisCardStatus.gameObject.transform.DOMove(cardBeingDroppedPosition, 0.2f, true);
			cardBeingDropped.transform.DOMove(thisCardPosition, 0.2f, true);

			int thisCardSlot = thisCardStatus.slotIsOn;
			thisCardStatus.slotIsOn = cardBeingDroppedStatus.slotIsOn;
			cardBeingDroppedStatus.slotIsOn = thisCardSlot;

			DeckBehaviour.Instance.onTable[thisCardSlot] = cardBeingDropped;
			DeckBehaviour.Instance.onTable[thisCardStatus.slotIsOn] = thisCardStatus.gameObject;

			CardModifier thisCardModifier = thisCardStatus.actualModifier;
			thisCardStatus.actualModifier = cardBeingDroppedStatus.actualModifier;
			cardBeingDroppedStatus.actualModifier = thisCardModifier;

			if (PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent <= 1)
				cardBeingDropped.GetComponent<IDragger>().OnEndDrag(null);

			GameManager.Instance.battleManager.TakePlayerActionPoints(1 - PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.SLOT_CHANGE_COST][0]);
			battleUIManagerScript.ShowActionPoints();
		}
	}
}
