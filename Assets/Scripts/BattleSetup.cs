﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   ScriptableObject com os dados de como deve ser uma determinada batalha*/

using UnityEngine;
using UnityEngine.UI;

public enum BattleConfig { ONE_ENEMY, TWO_ENEMIES, THREE_ENEMIES, ONE_BOSS, ONE_BOSS_TWO_HENCHMAN}

[CreateAssetMenu(fileName = "New BattleSetup", menuName = "BattleSetup")]
public class BattleSetup : ScriptableObject
{
	public BattleConfig config;

	public GameObject[] enemies; 

	public Image background; 

	public int coinsReward;
	public int xpReward;
}
