﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInImage : MonoBehaviour
{
    void OnEnable()
    {
        gameObject.GetComponent<Animator>().Play("FadeIn");
    }
}
