﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;

public class VictoryVisualEvents : MonoBehaviour
{
	public float animationDuration;
	public ParticleSystem victoryParticles;
	public BattleUIManager battleUIManager;
	public StudioEventEmitter eventEmitter;

	private float currentTime = 0.0f;
	private bool countTime = false;

	void OnEnable()
	{
		victoryParticles.gameObject.SetActive(true);
		victoryParticles.Pause();
	}

	public void StartAnimation()
	{
		victoryParticles.gameObject.SetActive(true);
		victoryParticles.Play(true);
		countTime = true;
	}

    void Update()
    {
		if (!countTime) return;

		if (currentTime > animationDuration)
		{
			victoryParticles.Pause();
			battleUIManager.BattleWonButton();
			countTime = false;
			currentTime = 0.0f;
		}

		currentTime += Time.deltaTime;
    }
}
