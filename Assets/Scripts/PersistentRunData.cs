﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RunData
{
	public int currentRun;
	public int maxRun = 5;
	public int level1EnemySpawnRate;
	public int level2EnemySpawnRate;
	public int level3EnemySpawnRate;
	public int[] level1EnemySpawnPerRun;
	public int[] level2EnemySpawnPerRun;
	public int[] level3EnemySpawnPerRun;
}

public class PersistentRunData : MonoBehaviour
{
	public RunData runData;

	public void SetNewGame()
	{
		runData.currentRun = 0;
		runData.level1EnemySpawnRate = 0;
		runData.level2EnemySpawnRate = 0;
		runData.level3EnemySpawnRate = 0;
	}

    public void AdvanceOneRun()
    {
		runData.currentRun++;

		if (runData.currentRun < runData.maxRun)
		{
			runData.level1EnemySpawnRate = runData.level1EnemySpawnPerRun[runData.currentRun];
			runData.level2EnemySpawnRate = runData.level2EnemySpawnPerRun[runData.currentRun];
			runData.level3EnemySpawnRate = runData.level3EnemySpawnPerRun[runData.currentRun];
		}
	}
}
