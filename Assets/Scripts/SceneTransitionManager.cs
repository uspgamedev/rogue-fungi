﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTransitionManager : MonoBehaviour
{
	public Animator transitionObjectAnimator;
	public AnimationClip transitionStartAnimation;
	public Button newGame;
	public Button loadGame;

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	int afterTransitionFunctionShopScene()
	{
		SceneManager.LoadScene("ShopScene");
		transitionObjectAnimator.SetBool("FullAnimation", true);
		return 0;
	}

	public void ChangeSceneToShopScene()
	{
		StartCoroutine(waitForTransition(transitionObjectAnimator, afterTransitionFunctionShopScene));
	}
	
	public void ChangeSceneToNewGameShopScene()
	{
		MenuData.doNewGame = true;
		MenuData.justCameFromMenu = true;
		newGame.enabled = false;
		StartCoroutine(waitForTransition(transitionObjectAnimator, afterTransitionFunctionShopScene));
	}

	int afterTransitionFunctionNewGame()
	{
		GameManager.Instance.state = GameState.MAP_SCREEN;
		SceneManager.LoadScene("MapScene");
		transitionObjectAnimator.SetBool("FullAnimation", true);
		return 0;
	}

	public void ChangeToSceneNewGame()
	{
		StartCoroutine(waitForTransition(transitionObjectAnimator, afterTransitionFunctionNewGame));
	}


	int afterTransitionFunctionLoadGame()
	{
		MenuData.doNewGame = false;
		MenuData.justCameFromMenu = true;
		GameManager.Instance.SetupLoadGame();
		GameManager.Instance.LoadGame();
		return 0;
	}

	public void ChangeToSceneLoadGame()
	{
		loadGame.enabled = false;
		StartCoroutine(waitForTransition(transitionObjectAnimator, afterTransitionFunctionLoadGame));
	}

	public IEnumerator waitForTransition(Animator transitionAnimator, Func<int> afterTransitionFunction)
	{
		transitionAnimator.SetTrigger("Start");

		yield return new WaitForSeconds(transitionStartAnimation.length);

		afterTransitionFunction();
	}

	public void ChangeBackToMapScene()
	{
		StartCoroutine(waitForTransition(transitionObjectAnimator, afterTransitionFunctionBackToMap));
	}

	private int afterTransitionFunctionBackToMap()
	{
		MenuData.doNewGame = false;
		MenuData.justCameFromMenu = false;
		MapSceneTransition.mapSceneTransitionData.needToEnableNextPlace = true;
		GameManager.Instance.state = GameState.MAP_SCREEN;

		SceneManager.LoadScene("MapScene");

		return 0;
	}

	public void BackToMenuScene()
	{
		GameManager.Instance.SaveGame();
		GameManager.Instance.SaveSettingsData();
		RestartButtonNoResetSave();
		SceneManager.LoadScene("MenuScene");
	}
	
	public void RestartButtonNoResetSave()
	{
		GameObject.Destroy(DeckBehaviour.Instance.gameObject);
		GameObject.Destroy(PlayerBehaviour.Instance.gameObject);
		GameObject.Destroy(GameManager.Instance.battleManager.gameObject);
		MapSceneTransition.ResetClass();
	}

}
