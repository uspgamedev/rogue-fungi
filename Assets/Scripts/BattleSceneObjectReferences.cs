﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BattleSceneObjectReferences : MonoBehaviour
{
	public GameObject battlePanel;
	public BattleUIManager battleUIManagerScript;
	public GameObject[] slots;
	public GameObject[] slotDescriptionImages;
	public TextMeshProUGUI slotLeftDescription;
	public TextMeshProUGUI slotCenterDescription;
	public TextMeshProUGUI slotRightDescription;

	[Header("Enemy Slots")]
	public GameObject enemySlot1_1;

	[Header("Enemy Actions")]
	public GameObject action;
	public ActionSlotBehaviour actionBehaviour;

	[Header ("Enemies")]
	public EnemyAction enemyActionScript;
	public EnemyActionPattern enemyActionPatternScript;

	[Header("UI")]
	public GameObject deckUIIcon;
}
