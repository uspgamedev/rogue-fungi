using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New BattleSetupPool", menuName = "BattleSetupPool")]
public class BattleSetupPool : ScriptableObject
{
	public BattleSetup[] battleSetupPoolLow;
	public BattleSetup[] battleSetupPoolMed;
	public BattleSetup[] battleSetupPoolHigh;
	public BattleSetup[] battleSetupPoolBoss;
	public int[] difficulties;
	public int[] indexes;
}
