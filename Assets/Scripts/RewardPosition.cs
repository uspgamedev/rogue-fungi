﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RewardType { CHEST, BATTLE, BOSS }
public class RewardPosition : MonoBehaviour
{
	public RewardType type;
	public GameObject selectionPanel;
	public bool isSelected = false;
	public int index;
	public IDragger iDraggerScript;

	public void OnEnable()
	{
		iDraggerScript.enabled = true;
	}
}
