﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSoundEmitter : MonoBehaviour
{
    public string fmodEventName;
    FMOD.Studio.EventInstance eventInstance;
    bool hasStarted = false;

    void Start()
    {
        eventInstance = FMODUnity.RuntimeManager.CreateInstance("event:/" + fmodEventName);
    }

    void Update()
    {
        var ps = GetComponent<ParticleSystem>();
        if (!hasStarted && ps.isPlaying)
        {
            eventInstance.start();
            hasStarted = true;
        }
        else if (hasStarted && !ps.isPlaying)
            hasStarted = false;
    }
}
