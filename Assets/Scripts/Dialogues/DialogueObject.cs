using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[System.Serializable]
public class DialogueObject
{	
	public string name;
	public DynamicLocaleDialogue[] lines;
	
}