﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOverTime : MonoBehaviour
{
	public float speed; // velocidade de rotação

	public void FixedUpdate()
	{
		Vector3 currentRot = transform.eulerAngles;
		Vector3 newRot = transform.eulerAngles + Vector3.forward * Time.deltaTime * speed;
		transform.eulerAngles = Vector3.Lerp(currentRot, newRot, 1);
	}
}
