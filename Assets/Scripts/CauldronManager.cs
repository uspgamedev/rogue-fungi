using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using TMPro;
using System.IO;
using DG.Tweening;

public class CauldronManager : MonoBehaviour
{
	[Header("Cauldron Settings")]
	public CauldronBuffsPack cauldronBuffsPack;
	public GameObject cauldronPanel;
	public GameObject[] cauldronButtons;
	public MoneyDisplay[] moneyDisplays;
	public List<int> cauldronBuffs = new List<int>();
	public List<int> availableBuffs = new List<int>();

	public void Awake()
	{
		GameManager.Instance.cauldronManager = this;

		if (CauldronData.cauldronBuffs != null) cauldronBuffs = CauldronData.cauldronBuffs;

		for(int i = 0; i < cauldronBuffsPack.cauldronBuffs.Length; i++)
			if(PlayerBehaviour.Instance.playerBuffs[cauldronBuffsPack.cauldronBuffs[i].type][1] < 10 &&
				cauldronBuffsPack.cauldronBuffs[i].buffUnlockAmount <= PlayerBehaviour.Instance.totalBuffs)
				availableBuffs.Add(i);
        EnterCauldron();
	}

	public void EnterCauldron()
	{
		RandomizeCauldron();
	}

	public void RandomizeCauldron()
	{
		int index = 0;

		foreach (GameObject cauldronButton in cauldronButtons)
		{

			CauldronBuffClass buffClass = null;

			if (CauldronData.cauldronBuffs == null)
			{
				if(availableBuffs.Count > 0)
				{
					int randomValue = Random.Range(0, availableBuffs.Count);
					buffClass = cauldronBuffsPack.cauldronBuffs[availableBuffs[randomValue]];
					cauldronBuffs.Add(availableBuffs[randomValue]);
					availableBuffs.RemoveAt(randomValue);
					index++;
				}
				else	cauldronButton.gameObject.SetActive(false);
			}
			else
			{
				buffClass = cauldronBuffsPack.cauldronBuffs[CauldronData.cauldronBuffs[index]];
				index++;
			}

			if(buffClass != null)
			{
				CauldronItem cauldronItemScript = cauldronButton.GetComponent<CauldronItem>();
				cauldronItemScript.cauldronBuff = buffClass;
				cauldronItemScript.price = buffClass.prices[PlayerBehaviour.Instance.playerBuffs[buffClass.type][1]];
				cauldronItemScript.priceText.text = buffClass.prices[PlayerBehaviour.Instance.playerBuffs[buffClass.type][1]].ToString();
				foreach(DynamicLocaleText localizedTexts in cauldronItemScript.cauldronBuff.localizedText)
				{
					if(LocalizationSettings.SelectedLocale.ToString() == localizedTexts.localeName.ToString())
					{
						cauldronItemScript.buffDescriptionText.text = localizedTexts.text;
						cauldronItemScript.buffDescriptionText.text = 
							cauldronItemScript.buffDescriptionText.text.Replace("&&", 
								buffClass.buffValues[PlayerBehaviour.Instance.playerBuffs[buffClass.type][1]].ToString());
						break;
					}
				}
				cauldronItemScript.cauldronManagerScript = this;
			}
		}
	}

}
