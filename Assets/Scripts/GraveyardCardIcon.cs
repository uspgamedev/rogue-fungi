﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GraveyardCardIcon : MonoBehaviour
{
	private BattleUIManager battleUIManager;
	[SerializeField] private SpriteRenderer spriteRenderer;
	[SerializeField] private SpriteRenderer glowSpriteRenderer;

	public void SetBattleUIManager(BattleUIManager battleUIManager)
	{
		this.battleUIManager = battleUIManager;
	}

	public void MoveTowardsDeck()
	{
		Vector3 targetPosition = battleUIManager.graveyardIconEndPosition.position;
		float movementSpeed = 0.4f - (DeckBehaviour.Instance.graveyardCards.Count * 0.025f);
		if (movementSpeed <= 0.0f) movementSpeed = 0.1f;
		transform.DOMove(targetPosition, movementSpeed).SetEase(Ease.InCirc).OnComplete(OnCompleteMoveToDeck);
		transform.DOScale(Vector3.zero, movementSpeed).SetEase(Ease.InCirc);
		glowSpriteRenderer.DOFade(1.0f, movementSpeed);
	}

	void OnCompleteMoveToDeck()
	{
		battleUIManager.deckButtonJuice.Juicy(1.4f);
		DOTween.Kill(transform);
		Destroy(gameObject);
	}
}
