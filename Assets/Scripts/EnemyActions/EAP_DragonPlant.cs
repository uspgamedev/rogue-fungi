using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EAP_DragonPlant : EnemyActionPattern
{
	public int[] strengthPerStage;
	private IEnumerator nextMove;

	public override IEnumerator ApplyAction()
	{
		return nextMove;
	}

	public override void ActionPatternStage1()
	{
		switch(enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				enemyActionPatternData.count++;
				break;
			case 3:
				enemyAction.PrepareToGainStrength(strengthPerStage[0]);
				nextMove = enemyAction.GainStrength(strengthPerStage[0]);
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage2()
	{
		switch(enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.RemoveGainedStrength();
				// 50% Protege vs 50% Bagunça
				float randomNumber = Random.value;
				if(randomNumber <= 0.5)
				{
					enemyAction.PrepareToClumsiness(3);
					nextMove = enemyAction.Clumsiness(3);
				}
				else
				{
					enemyAction.PrepareToProtect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
					nextMove = enemyAction.Protect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				}

				enemyActionPatternData.count++;
				break;
			case 3:
				// Presente Desagradavel
				enemyAction.PrepareToGainStrength(strengthPerStage[1]);
				nextMove = enemyAction.GainStrength(strengthPerStage[1]);
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage3()
	{
		switch(enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 2:
				// 70% Ataca vs 30% Bagunça
				float randomNumber = Random.value;
				if(randomNumber <= 0.3)
				{
					enemyAction.PrepareToClumsiness(1);
					nextMove = enemyAction.Clumsiness(1);
				}
				else
				{
					enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
					nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				}

				enemyActionPatternData.count++;
				break;
			case 3:
				enemyAction.RemoveGainedStrength();
				enemyAction.PrepareToGainStrength(strengthPerStage[2]);
				nextMove = enemyAction.GainStrength(strengthPerStage[2]);
				enemyActionPatternData.count = 1;
				break;
		}
	}
}
