﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EA_Tree : EnemyActionPattern
{

	public override void DecideAction()
	{
		if (GameManager.Instance.battleManager.round % 2 == 0)
			enemyAction.PrepareAttack(GameManager.Instance.battleManager.enemyUnitScript.enemyUnitData.actualAttack1);
		else
			enemyAction.PrepareAttack(GameManager.Instance.battleManager.enemyUnitScript.enemyUnitData.actualAttack1 + 1);
	}

	public override IEnumerator ApplyAction()
	{
		if (GameManager.Instance.battleManager.round % 2 == 0)
			yield return StartCoroutine(enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1));
		else
			yield return StartCoroutine(enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1 + 1));
	}
}
