/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Script que cuida do que acontece durante a batalha
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Localization.SmartFormat.PersistentVariables;
using DG.Tweening;

public enum BattleState { START, PLAYERTURN, ENEMYTURN, WON, LOST }
[Serializable] public enum CardModifier { ORIGINAL, POISON, PROTECTION, PIERCING, FISICAL_DMG, MAGIC_DMG } // adicionar mais conforme crio os efeitos

public class BattleManager : MonoBehaviour
{
	[Header("General")]
	public int round;
	public BattleState state;
	public bool outOfCards = false;
	public bool applyingPlayerStatusEffects = false;
	private int emptySlots = 0;
	public int cardActivationY = -40;
	public int destructionLasts = 0;
	public bool cardSelectEvent = false;
	public bool allowCardRefill = true;
	public BattleUIManager battleUIManager;

	[Header("Global bonuses")]
	public int bonusDamage = 0;
	public int precautionsTaken = 0;

	[Header("Turn bonuses")]
	public int bonusDamageThisTurn = 0; 
	public int damageMultiplierNextAttack = 1;

	[Header("Essential Scripts")]
	public BattleSceneObjectReferences objectReferences;
	public int slotBlocked = -1;
	public GameObject[] cardsAtSlots;

	[Header("Battle Setup")]
	public BattleSetup thisBattleSetup; 

	[Header ("Enemies")]
	public GameObject[] enemyCard;
	public int enemiesKilled;
	public int totalEnemyStages;
	public EnemyBehaviour enemyBehaviourScript;
	public EnemyDisplay enemyDisplayScript;
	public EnemyUnit enemyUnitScript;

	void Awake()
	{
		GameManager.Instance.battleManager = this;
		thisBattleSetup = GameManager.Instance.currentBattleSetup;
		totalEnemyStages = GameManager.Instance.enemyMax;
	}

	public void SetupBattleReferences()
	{
		enemyCard = new GameObject[3]; 
		enemiesKilled = 0;
		cardsAtSlots = new GameObject[3];
	}

	public void ResetBattleState()
	{
		state = BattleState.START;
	}

	public void SetBattleState(BattleState s)
	{
		if (state == BattleState.LOST)
			Logging.Log("Battle State Lost não vou fazer nada");
		else  state = s;
	}

	public void StartBattle()
	{
		round = 0;
		GameManager.Instance.state = GameState.BATTLE_SCREEN;

		PlayerBehaviour.Instance.playerBehaviourData.actionPointsInitial += 
			PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.ACTION_POINTS][0];
		PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.ACTION_POINTS][0] = 0;

		PlayerBehaviour.Instance.playerBehaviourData.lifeInitial += 
			PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.MAX_HEALTH_POINTS][0];
		PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.MAX_HEALTH_POINTS][0] = 0;

		PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent = 
			PlayerBehaviour.Instance.playerBehaviourData.actionPointsInitial + 
			PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.FIRST_TURN_ACTION][0];
			
		objectReferences.battleUIManagerScript.ShowActionPoints();
		objectReferences.battleUIManagerScript.ShowLifePoints();

		SetupEnemies();

		foreach (string cardName in DeckBehaviour.Instance.deckList)
		{
			GameObject card = GameManager.GetCardFromBundle(cardName);
			GameObject cardCreated = Instantiate(card, objectReferences.battleUIManagerScript.battleUI.transform, false);
			CardStatus cardStatus = cardCreated.GetComponent<CardStatus>();
			cardStatus.indexOnDeck = DeckBehaviour.Instance.deckInstantiatedList.Count;
			Logging.Log("Index: " + cardStatus.indexOnDeck);
			Logging.Log(card.name);
			int upgrades = DeckBehaviour.Instance.levelOfEachCard[cardStatus.indexOnDeck] - cardStatus.level;
			Logging.Log("Upgrades: " + upgrades);
			
			for (int i = 0; i < upgrades; i++)
			{
				Logging.Log("Upgrade em: " + cardStatus.indexOnDeck);
				UpgradeManager.UpgradeCard(cardStatus);
			}

			if(cardName == "ATQ_TinyFriends")	DeckBehaviour.Instance.troopCount++;

			DeckBehaviour.Instance.deckInstantiatedList.Add(cardCreated);
			cardCreated.GetComponent<IDragger>().SendCardOffScreen();
		}

		DeckBehaviour.Instance.ShuffleCardsToDeck(DeckBehaviour.Instance.deckInstantiatedList, DeckBehaviour.Instance.deckInstantiatedQueue);

		objectReferences.enemyActionPatternScript.DecideAction();

		SetBattleState(BattleState.PLAYERTURN);
		for(int i = 0; i < 3; i++) AddCardToSlot(objectReferences.slots[i]);

		GameManager.Instance.SaveGame();
	}

	public void LoadBattle(SaveData dataToLoad)
	{
		DeckBehaviour.Instance.deckInstantiatedList = new List<GameObject>();
		DeckBehaviour.Instance.onTable = new GameObject[3];
		for (int i = 0; i < dataToLoad.deckInstantiatedList.Count; i++)
		{
			GameObject cardPrefab = GameManager.GetCardFromBundle(dataToLoad.deckInstantiatedList[i]);
			GameObject instantiatedCard = Instantiate(cardPrefab, battleUIManager.battleUI.transform, false);
			DeckBehaviour.Instance.deckInstantiatedList.Add(instantiatedCard);

			CardStatus cardStatus = instantiatedCard.GetComponent<CardStatus>();
			cardStatus.indexOnDeck = i;
			Logging.Log("tamanho total " + dataToLoad.levelOfEachCard.Count);
			Logging.Log("indice " + cardStatus.indexOnDeck);
			int upgrades = dataToLoad.levelOfEachCard[cardStatus.indexOnDeck] - cardStatus.level;
			for (int j = 0; j < upgrades; j++)	UpgradeManager.UpgradeCard(cardStatus);

			instantiatedCard.GetComponent<IDragger>().SendCardOffScreen();

			for (int j = 0; j < dataToLoad.cardsOnTable.Length; j++)
			{
				Logging.Log("Indice da carta no deck: " + i);
				Logging.Log("Indice da carta na mesa: " + dataToLoad.cardsOnTable[j]);
				if (i == dataToLoad.cardsOnTable[j])
				{
					Logging.Log("Tentou ativar");

					cardStatus.actualModifier = dataToLoad.cardModifiers[j];
					cardStatus.slotIsOn = dataToLoad.slots[j];
					instantiatedCard.transform.position = GameManager.Instance.battleManager.objectReferences.slots[cardStatus.slotIsOn].transform.position;
					DeckBehaviour.Instance.onTable[j] = instantiatedCard;
					GameManager.Instance.battleManager.cardsAtSlots[j] = instantiatedCard;
					instantiatedCard.GetComponent<IDragger>().effectExplainerIndex = j;
				}
			}
		}

		DeckBehaviour.Instance.deckInstantiatedQueue = new Queue<GameObject>();
		foreach (int cardIndex in dataToLoad.deckInstantiatedQueue)	DeckBehaviour.Instance.deckInstantiatedQueue.Enqueue(DeckBehaviour.Instance.deckInstantiatedList[cardIndex]);

		battleUIManager.ShowDeckNumberCards();
		battleUIManager.ShowGraveyardNumberCards();
		
		DeckBehaviour.Instance.graveyardCards = new Queue<GameObject>();
		foreach (int cardIndex in dataToLoad.graveyardCards)	DeckBehaviour.Instance.graveyardCards.Enqueue(DeckBehaviour.Instance.deckInstantiatedList[cardIndex]);

		for (int modifier = 0; modifier < 3; modifier++)	GameManager.Instance.battleManager.ApplySlotModifier(modifier, dataToLoad.slotModifiers[modifier], dataToLoad.slotModifiersPower[modifier]);

		LoadPlayerBattleStatus();
		LoadEnemy(dataToLoad);
	}

	private void LoadPlayerBattleStatus()
	{
		battleUIManager.ShowPlayerStatus();
	}

	private void LoadEnemy(SaveData dataToLoad)
	{
		GameObject enemyPrefab = GameManager.GetEnemyFromBundle(dataToLoad.enemyCard);
		GameObject enemy = Instantiate(enemyPrefab, GameManager.Instance.battleManager.objectReferences.enemySlot1_1.transform, false);

		GameManager.Instance.battleManager.enemyBehaviourScript = enemy.GetComponent<EnemyBehaviour>();
		GameManager.Instance.battleManager.enemyDisplayScript = enemy.GetComponent<EnemyDisplay>();
		GameManager.Instance.battleManager.objectReferences.enemyActionPatternScript = enemy.GetComponent<EnemyActionPattern>();
		GameManager.Instance.battleManager.enemyUnitScript = enemy.GetComponent<EnemyUnit>();

		GameManager.Instance.battleManager.objectReferences.enemyActionScript.enemyUnit = GameManager.Instance.battleManager.enemyUnitScript;
		GameManager.Instance.battleManager.enemyCard[0] = enemy;
		GameManager.Instance.battleManager.objectReferences.enemyActionPatternScript.enemyActionPatternData.enemyMaxStage = GameManager.Instance.enemyMax;

		GameManager.Instance.battleManager.enemyUnitScript.enemyUnitData = dataToLoad.enemyUnitData;
		enemy.transform.localScale = new Vector3(GameManager.Instance.battleManager.enemyUnitScript.enemyUnitData.scale, GameManager.Instance.battleManager.enemyUnitScript.enemyUnitData.scale, 1f);
		GameManager.Instance.battleManager.objectReferences.enemyActionPatternScript.enemyActionPatternData = dataToLoad.enemyActionPatternData;

		battleUIManager.ShowEnemyStatus();
		battleUIManager.enemyHealthBar.SetMaxHealthValue(GameManager.Instance.battleManager.enemyUnitScript.enemyUnitData.originalHealth);
		battleUIManager.enemyHealthBar.SetHealth(GameManager.Instance.battleManager.enemyUnitScript.enemyUnitData.actualHealth);
	}

	IEnumerator InstantiateGraveyardIcons(int amount)
	{
		for(int counter = 0; counter < amount; counter++)
		{
			GameObject cardIcon = Instantiate(battleUIManager.graveyardCardIcon,
												battleUIManager.graveyardIconSpawnPosition.position,
												battleUIManager.graveyardIconSpawnPosition.rotation,
												battleUIManager.battleUI.transform);

			cardIcon.transform.localScale = new Vector3(10.0f, 10.0f, 10.0f);

			GraveyardCardIcon cardIconScript = cardIcon.GetComponent<GraveyardCardIcon>();
			cardIconScript.SetBattleUIManager(battleUIManager);
			cardIconScript.MoveTowardsDeck();

			yield return new WaitForSeconds(0.2f);
		}

		yield break;
	}

	public GameObject AddCardToSlot(GameObject slot)
	{
		if (state == BattleState.LOST) return null;

		if (!allowCardRefill) return null;
		
		SlotBehaviour thisSlotBehaviourScript = slot.GetComponent<SlotBehaviour>();
		if (PlayerBehaviour.Instance.playerBehaviourData.curseCurrent > 0)
		{
			if (slotBlocked != -1)	SlotClear(slotBlocked);
			slotBlocked = (int) thisSlotBehaviourScript.slot;
			SlotBlock((int) thisSlotBehaviourScript.slot);
		}

		GameObject cardCreated = DeckBehaviour.Instance.deckInstantiatedQueue.Dequeue();
		CardStatus thisCardStatusScript = cardCreated.GetComponent<CardStatus>();
		thisCardStatusScript.slotIsOn = (int) thisSlotBehaviourScript.slot;

		if (DeckBehaviour.Instance.deckInstantiatedQueue.Count == 0)
		{
			if (DeckBehaviour.Instance.graveyardCards.Count == 0)
			{
				emptySlots += 1;
				if (emptySlots == 3)	outOfCards = true;

				Logging.Log("Cemitério vazio...");
				return null;
			}

			IEnumerator coroutine = InstantiateGraveyardIcons(DeckBehaviour.Instance.graveyardCards.Count);
			StartCoroutine(coroutine);
			DeckBehaviour.Instance.ShuffleGraveyardToDeck();
			objectReferences.battleUIManagerScript.ShowDeckNumberCards();
			objectReferences.battleUIManagerScript.ShowGraveyardNumberCards();
		}
		
		switch (thisSlotBehaviourScript.slot)
		{
			case SlotPosition.RIGHT:
				cardsAtSlots[2] = cardCreated;
				break;

			default:
				Logging.LogWarning("O SLOT NAO ESTA DEFINIDO");
				break;
		}

		cardCreated.transform.position = objectReferences.deckUIIcon.transform.position;
		cardCreated.transform.DOMove(objectReferences.slots[thisCardStatusScript.slotIsOn].transform.position, 0.2f, true);
		Debug.Log(cardCreated.transform.position);
		Debug.Log(slot.transform.position);

		cardCreated.GetComponent<CardsEffects>().OnDrawEffect();
		objectReferences.deckUIIcon.GetComponent<ButtonJuicy>().Juicy(0.4f);

		thisCardStatusScript.actualModifier = objectReferences.slots[thisCardStatusScript.slotIsOn].GetComponent<SlotBehaviour>().actualModifier;
		DeckBehaviour.Instance.onTable[thisCardStatusScript.slotIsOn] = cardCreated;

		cardCreated.GetComponent<IDragger>().effectExplainerIndex = thisCardStatusScript.slotIsOn;

		objectReferences.battleUIManagerScript.ShowDeckNumberCards();
		objectReferences.battleUIManagerScript.ShowGraveyardNumberCards();
		cardCreated.transform.localScale = new Vector3(0.1f, 0.1f, 1f);

		return cardCreated;
	}

	public void SetupEnemies()
	{
		int enemyMaxStage = GameManager.Instance.enemyMax;
		Logging.Log("Enemy max stage: " + enemyMaxStage);
		CreateEnemy(0, objectReferences.enemySlot1_1, enemyMaxStage);
		totalEnemyStages = enemyMaxStage;
	}

	public void CreateEnemy(int arrayPosition, GameObject slot, int enemyMaxStage) 
	{
		GameObject enemy = Instantiate(thisBattleSetup.enemies[arrayPosition], slot.transform, false);

		enemyBehaviourScript = enemy.GetComponent<EnemyBehaviour>();
		enemyUnitScript = enemy.GetComponent<EnemyUnit>();

		objectReferences.enemyActionScript.enemyUnit = enemyUnitScript;
		objectReferences.enemyActionPatternScript = enemy.GetComponent<EnemyActionPattern>();
		enemyDisplayScript = enemy.GetComponent<EnemyDisplay>();
		
		objectReferences.enemyActionPatternScript.enemyActionPatternData.enemyMaxStage = enemyMaxStage;
		enemy.transform.localScale = new Vector3(enemyUnitScript.enemyUnitData.scale, enemyUnitScript.enemyUnitData.scale, 1f);
		enemyCard[arrayPosition] = enemy;

		objectReferences.battleUIManagerScript.ShowEnemyLifePoints();
		objectReferences.battleUIManagerScript.ResetDebuffSlots();
	}

	public void EndPlayerTurn()
	{
		ApplyPassiveEffects();
		ResetTurnBonuses();
		if (state != BattleState.WON && state != BattleState.LOST)
		{
			SetBattleState(BattleState.ENEMYTURN); 
			StartCoroutine(EnemyTurn());
		}
	}

	public void TakePlayerActionPoints(int actionPoints)
	{
		if (PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent > 0)
			PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent -= actionPoints; 
		if (!cardSelectEvent && PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent == 0)	
			EndPlayerTurn(); 
	}

	IEnumerator EnemyTurn()
	{
		if (state != BattleState.WON && state != BattleState.LOST)
		{
			DeckBehaviour.Instance.ResetCardPositions();
			if (PlayerBehaviour.Instance.playerBehaviourData.curseCurrent > 0)
			{
				PlayerBehaviour.Instance.playerBehaviourData.curseCurrent--;
				slotBlocked = -1;
				objectReferences.battleUIManagerScript.ShowPlayerCurse();
			}
			else	slotBlocked = -1;

			for (int i = 0; i < 3; i++)
			{
				if (DeckBehaviour.Instance.onTable[i] != null)
					DeckBehaviour.Instance.onTable[i].GetComponent<EventTrigger>().enabled = false;
				SlotClear(i);
			}	

			enemyUnitScript.enemyUnitData.actualProtection = 0;
			objectReferences.battleUIManagerScript.ShowEnemyProtection();
			objectReferences.battleUIManagerScript.OnOvershadowPlayerTable();

			objectReferences.battleUIManagerScript.StartTurnTransitionAnimation("TurnTransition/EnemyTurn");
			yield return new WaitForSeconds(objectReferences.battleUIManagerScript.turnAnimationClip.length);
			objectReferences.battleUIManagerScript.DisableTurnTransitionAnimation();

			objectReferences.battleUIManagerScript.DecreaseSlotTransparency();

			if (destructionLasts > 0)	DestroyCards();

			CheckAndApplyEnemyCurses(enemyCard[0].GetComponent<EnemyBehaviour>());

			if (state == BattleState.LOST || state == BattleState.WON) yield break;

			yield return new WaitForSeconds(0.5f);

			if (enemyCard[0] != null)	StartCoroutine(objectReferences.enemyActionPatternScript.ApplyAction());

			if (PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent <= 0)	objectReferences.battleUIManagerScript.PlayerLose();

			yield return new WaitForSeconds(2.5f);

			if (state == BattleState.LOST || state == BattleState.WON) yield break;

			if (PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent <= 0)
			{
				objectReferences.battleUIManagerScript.PlayerLose();
				yield break;
			}

			objectReferences.battleUIManagerScript.StartTurnTransitionAnimation("TurnTransition/PlayerTurn");
			yield return new WaitForSeconds(objectReferences.battleUIManagerScript.turnAnimationClip.length);
			objectReferences.battleUIManagerScript.DisableTurnTransitionAnimation();


			objectReferences.battleUIManagerScript.OffOvershadowPlayerTable();
			objectReferences.battleUIManagerScript.IncreaseSlotTransparency();

			round++;
			if (--enemyBehaviourScript.specialProtectionTurns < 0)
			{
				enemyUnitScript.enemyUnitData.actualSpecialProtection = 0;
				objectReferences.battleUIManagerScript.ShowEnemySpecialProtection();
			}

			if (state != BattleState.WON && state != BattleState.LOST)	yield return StartCoroutine(StartPlayerTurn(true));
			else	yield return null;
		}
	}

	public IEnumerator StartPlayerTurn(bool saveGame)
	{
		if (state == BattleState.WON || state == BattleState.LOST)	yield break;
		SetBattleState(BattleState.PLAYERTURN);

		if (saveGame)	GameManager.Instance.SaveGame();
		
		for (int i = 0; i < 3; i++)
			if (DeckBehaviour.Instance.onTable[i] != null)
				DeckBehaviour.Instance.onTable[i].GetComponent<EventTrigger>().enabled = true;

		objectReferences.battleUIManagerScript.enemyHealthBar.SetHealth(enemyUnitScript.enemyUnitData.actualHealth);
		Logging.Log("VIDA DO INIMIGO: " + objectReferences.battleUIManagerScript.enemyHealthBar.slider.value);

		objectReferences.enemyActionPatternScript.DecideAction();
		CheckAndApplyPlayerCurses();

		PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent = 
			PlayerBehaviour.Instance.playerBehaviourData.actionPointsInitial;

		if (PlayerBehaviour.Instance.playerBehaviourData.tirednessTurns > 0)
		{
			yield return StartCoroutine(objectReferences.battleUIManagerScript.DrainActionPoints());
			PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent -= PlayerBehaviour.Instance.playerBehaviourData.tirednessCurrent;
			PlayerBehaviour.Instance.playerBehaviourData.tirednessTurns--;
			objectReferences.battleUIManagerScript.ShowPlayerTiredness();
			yield return new WaitForSeconds(0.2f);
		}

		objectReferences.battleUIManagerScript.ShowActionPoints();

		if (PlayerBehaviour.Instance.playerBehaviourData.clumsinessCurrent > 0)
		{
			yield return StartCoroutine(DeckBehaviour.Instance.SwitchUpCards());
			PlayerBehaviour.Instance.playerBehaviourData.clumsinessCurrent--;
			objectReferences.battleUIManagerScript.ShowPlayerClumsiness();
		}

		PlayerBehaviour.Instance.playerBehaviourData.defenseCurrent = 0;
		objectReferences.battleUIManagerScript.ShowPlayerProtection();

		if (outOfCards)	TakePlayerActionPoints(PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent);

		Logging.Log("VIDA DO INIMIGO: " + objectReferences.battleUIManagerScript.enemyHealthBar.slider.value);
	}

	public void PassPlayerTurn()
	{
		if (state != BattleState.WON && state != BattleState.LOST && state == BattleState.PLAYERTURN)
		{
			TakePlayerActionPoints(PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent);
			objectReferences.battleUIManagerScript.ShowActionPoints();
		}
	}

	public void ApplyPassiveEffects()
	{
		foreach (GameObject card in DeckBehaviour.Instance.onTable)
			foreach (CardsEffects effect in card.GetComponent<CardStatus>().cardEffects)
				if (effect != null && effect.hasPassiveEffect)	
					effect.ApplyPassiveEffect();
	}

	public void CheckAndApplyPlayerCurses()
	{
		if (PlayerBehaviour.Instance.playerBehaviourData.poisonCurrent > 0)	
			PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent -= PlayerBehaviour.Instance.playerBehaviourData.poisonCurrent;
			PlayerBehaviour.Instance.playerBehaviourData.poisonCurrent -= 1;
	}

	public void CheckAndApplyEnemyCurses(EnemyBehaviour enemy)
	{
		if (enemy.thisEnemyUnit.enemyUnitData.actualPoison > 0)	enemy.TakePoisonDamage();
	}

	public void CheckEnemiesKilled()
	{
		Logging.Log("enemiesKilled " + enemiesKilled);
		Logging.Log("totalEnemyStages " + totalEnemyStages);

		if (enemiesKilled > 0)
		{
			SetBattleState(BattleState.WON);
			Logging.Log("Vitória normal");
			DeactivateCardsExcept(-1);
			objectReferences.battleUIManagerScript.thisBattleShrooms = thisBattleSetup.coinsReward;
			objectReferences.battleUIManagerScript.PlayerWon();
			objectReferences.battleUIManagerScript.DisableExplosions();	
		}
	}

	public void ResetSlotModifiers()
	{
		foreach (GameObject slot in objectReferences.slots)
		{
			SlotBehaviour slotBehaviour = slot.GetComponent<SlotBehaviour>();
			slotBehaviour.actualModifier = CardModifier.ORIGINAL;
			slotBehaviour.slotImage.color = Color.white;
			slotBehaviour.modifierImage.SetActive(false);
		}
	}

	public void ApplySlotModifier(int index, CardModifier mod, int power)
	{
		SlotBehaviour thisSlotBehaviour = objectReferences.slots[index].GetComponent<SlotBehaviour>();

		thisSlotBehaviour.actualModifier = mod;
		thisSlotBehaviour.slotImage.color = objectReferences.battleUIManagerScript.GetColorFromMod(mod);
		thisSlotBehaviour.modifierImage.SetActive(true);
		thisSlotBehaviour.modifierImage.GetComponent<Image>().sprite = objectReferences.battleUIManagerScript.GetSpriteFromMod(mod);
		thisSlotBehaviour.modifierImage.GetComponent<Outline>().effectColor = objectReferences.battleUIManagerScript.GetColorFromMod(mod);
		thisSlotBehaviour.powerSlot = power;

		switch(mod)
		{
			case CardModifier.FISICAL_DMG:
				thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Strength");
				break;
			case CardModifier.POISON:
				thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Poison");
				break;
			case CardModifier.PROTECTION:
				thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Protection");	
				break;
			case CardModifier.PIERCING:
				thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Piercing");
				break;
			default:
				thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Default");
				break;
		}

		thisSlotBehaviour.localizedString.StringReference.Add("v", new ObjectVariable {Value = thisSlotBehaviour});
	}

	private void ResetGlobalBonuses()
	{
		bonusDamage = 0;
		precautionsTaken = 0;
		objectReferences.battleUIManagerScript.ShowPlayerBonusDamage();
	}
	private void ResetTurnBonuses()
	{
		bonusDamage -= bonusDamageThisTurn;
		if (bonusDamage < 0)	bonusDamage = 0;
		bonusDamageThisTurn = 0;
		damageMultiplierNextAttack = 1;
		objectReferences.battleUIManagerScript.ShowPlayerDamageMultiplier();
		objectReferences.battleUIManagerScript.ShowPlayerBonusDamage();
	}

	public void LostBattle()
	{
		DeactivateCardsExcept(-1);
		SetBattleState(BattleState.LOST);
	}

	public void FinishBattle()
	{
		DeckBehaviour.Instance.ResetCardPositions();

		ResetBattleState(); 

		DeckBehaviour.Instance.CleanTable();
		DeckBehaviour.Instance.ResetDeck();
		DeckBehaviour.Instance.troopCount = 0;

		for(int card = 0; card < 3; card++) Destroy(cardsAtSlots[card]);

		ResetSlotModifiers();
		objectReferences.battleUIManagerScript.ResetSlotDescriptions();

		objectReferences.battleUIManagerScript.ResetDebuffSlots();

		round = 0;

		objectReferences.enemyActionScript.RemoveGainedStrength();

		Destroy(enemyCard[0]);
		enemiesKilled = 0;

		ResetGlobalBonuses();
		ResetTurnBonuses();

		slotBlocked = -1;

		PlayerBehaviour.Instance.ResetPlayerStatus();

		objectReferences.battleUIManagerScript.DisableAllPlayerStatusIcons();

		objectReferences.battleUIManagerScript.ShowPlayerStatus();

		objectReferences.battleUIManagerScript.ShowEnemyStatus();
		objectReferences.battleUIManagerScript.ShowEnemyPoison();

		for(int block = 0; block < 3; block++) objectReferences.battleUIManagerScript.slotBlockOverlay[block].SetActive(false);

		objectReferences.battleUIManagerScript.dmgTakenText.SetActive(false);
		objectReferences.battleUIManagerScript.DisableExplosions();

		Debug.Log(PlayerBehaviour.Instance.playerBehaviourData.coins);
		Debug.Log(thisBattleSetup == null);
		PlayerBehaviour.Instance.playerBehaviourData.coins += thisBattleSetup.coinsReward;

		if(PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent +
			(PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.AFTER_BATTLE_RECOVERY][0]) < 
				PlayerBehaviour.Instance.playerBehaviourData.lifeInitial)
				PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent +=
				(PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.AFTER_BATTLE_RECOVERY][0]);
		else PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent = PlayerBehaviour.Instance.playerBehaviourData.lifeInitial;

		objectReferences.battleUIManagerScript.DisableAllPlayerIcons();
		objectReferences.battleUIManagerScript.DisableAllEnemyIcons();
		DeckBehaviour.Instance.DestroyCardsToDestroyAtEnd();
	}

	public int FindIndexOnTable(GameObject cardSelected)
	{
		for (int cardIndex = 0; cardIndex < DeckBehaviour.Instance.onTable.Length; cardIndex++)
			if (DeckBehaviour.Instance.onTable[cardIndex] == cardSelected)	return cardIndex;

		return -1;
	}

	public void DeactivateCardsExcept(int index)
	{
		Logging.Log("Desativa exceto " + index);
		for (int cardIndex = 0; cardIndex < DeckBehaviour.Instance.onTable.Length; cardIndex++)
			if (cardIndex != index)
				DeckBehaviour.Instance.onTable[cardIndex].GetComponent<IDragger>().cardDetectionImage.raycastTarget = false;
	}

	public void DeactivateCardAt(int index)
	{
		IDragger cardIDragger = DeckBehaviour.Instance.onTable[index].GetComponent<IDragger>();
		cardIDragger.cardDetectionImage.raycastTarget = false;
	}

	public void ActivateCardAt(int index)
	{
		IDragger cardIDragger = DeckBehaviour.Instance.onTable[index].GetComponent<IDragger>();
		cardIDragger.cardDetectionImage.raycastTarget = true;
	}

	public void DeactivateSlot(int index)
	{
		objectReferences.slots[index].GetComponent<SlotBehaviour>().enabled = false;
	}

	public void ActivateSlots()
	{
		foreach(GameObject slot in objectReferences.slots)	slot.GetComponent<SlotBehaviour>().enabled = true;
	}

	public void ActivateCards()
	{
		if(GameManager.Instance.state == GameState.BATTLE_SCREEN)
		{
			Logging.Log("Ativa");
			for (int cardIndex = 0; cardIndex < DeckBehaviour.Instance.onTable.Length; cardIndex++)
			{
				if (cardIndex != slotBlocked)
				{
					Logging.Log("Carta " + cardIndex + DeckBehaviour.Instance.onTable[cardIndex]);
					IDragger cardIDragger = DeckBehaviour.Instance.onTable[cardIndex].GetComponent<IDragger>();
					cardIDragger.cardDetectionImage.raycastTarget = true;
				}
			}
		}
	}

	public void DestroyCardAt(int index)
	{
		Logging.Log("Destroy card at: " + index);
		GameObject cardCurrent = DeckBehaviour.Instance.onTable[index];
		DeckBehaviour.Instance.graveyardCards.Enqueue(cardCurrent);
		foreach (CardsEffects eff in cardCurrent.GetComponent<CardStatus>().cardEffects)	eff.OnDiscardEffect();
		cardCurrent.GetComponent<IDragger>().SendCardOffScreen();

		GameObject cardCreated = AddCardToSlot(objectReferences.slots[index]);
		Logging.Log("Card created: " + cardCreated);

		objectReferences.battleUIManagerScript.DestroyCard(index);

		if (objectReferences.battleUIManagerScript.slotBlockOverlay[index].activeSelf)	SlotBlock(index);
	}

	private void DestroyCards()
	{
		for (int index = 0; index <= 2; index++)	DestroyCardAt(index);
		destructionLasts--;
		objectReferences.battleUIManagerScript.ShowDestruction();
	}

	public void EnemyAdvanceStage()
	{
		int enemyStage = ++objectReferences.enemyActionPatternScript.enemyActionPatternData.enemyStage;
		Logging.Log("Enemy Stage " + enemyStage);
		objectReferences.enemyActionPatternScript.enemyActionPatternData.count = 1;
		enemyUnitScript.enemyUnitData.currentStage = enemyStage;
		enemyUnitScript.actionPattern.enemyActionPatternData.count = 1;
		enemyUnitScript.enemyUnitData.actualAttack1 = enemyUnitScript.enemyUnitData.attack1PerStage[enemyStage];
		enemyUnitScript.enemyUnitData.actualAttack2 = enemyUnitScript.enemyUnitData.attack2PerStage[enemyStage];
		enemyUnitScript.enemyUnitData.actualProtectionPower = enemyUnitScript.enemyUnitData.protectionPerStage[enemyStage];
		enemyUnitScript.enemyUnitData.actualSpecialProtection = enemyUnitScript.enemyUnitData.specialProtectionPerStage[enemyStage];
	}

	public void SlotBlock(int index)
	{
		DeactivateCardAt(index);
		objectReferences.battleUIManagerScript.slotBlockOverlay[index].SetActive(true);
		objectReferences.battleUIManagerScript.slotBlockOverlay[index].transform.SetAsLastSibling();
	}

	public void SlotClear(int index)
	{
		ActivateCardAt(index);
		objectReferences.battleUIManagerScript.slotBlockOverlay[index].SetActive(false);
	}

	private void FixDeckUIScale()
	{
		objectReferences.deckUIIcon.GetComponent<RectTransform>().localScale = new Vector3(0.3f, 0.3f, 0f);
	}
}
