﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Programa com o comportamento do deck do jogador*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class DeckBehaviour : MonoBehaviour
{
	public static DeckBehaviour Instance { get; private set; }

	public List<GameObject> initialDeckCards;
	public List<string> deckList;
	public int troopCount = 0;
	public List<int> levelOfEachCard;
	public List<GameObject> deckInstantiatedList;
	public Queue<GameObject> graveyardCards;
	public Queue<GameObject> deckQueue;
	public Queue<GameObject> deckInstantiatedQueue;
	public List<GameObject> destroyAtEndOfBattle;
	public GameObject[] onTable; 
	public DeckVisualizer visualizer;
	public int levelArrayToDelete = 0;

	private void Awake()
    {

        if (Instance != null && Instance != this) Destroy(gameObject);
        else Instance = this;
        DontDestroyOnLoad(gameObject);
        NewRunDeck();
    }

    public void NewRunDeck()
    {
        levelOfEachCard = new List<int>();
        deckQueue = new Queue<GameObject>();
        graveyardCards = new Queue<GameObject>();
        destroyAtEndOfBattle = new List<GameObject>();
        deckInstantiatedQueue = new Queue<GameObject>();
		deckList = new List<string>();
        
		foreach(GameObject card in initialDeckCards)
		{
			deckList.Add(card.name);
			levelOfEachCard.Add(0);
		}
    }

	public void DestroyCardsToDestroyAtEnd()
	{
		foreach (GameObject obj in destroyAtEndOfBattle) if (obj != null)	Destroy(obj);
		destroyAtEndOfBattle.Clear();
	}

	public void ShuffleCardsToDeck(List<GameObject> cards, Queue<GameObject> deck)
	{
		int randomNumber = 0;

		for (int i = 0; i < cards.Count; i++)
		{
			randomNumber = Random.Range(0, (cards.Count - 1));

			GameObject temp = cards[randomNumber];
			cards[randomNumber] = cards[i];
			cards[i] = temp;
		}

		for (int i = 0; i < cards.Count; i++)	deck.Enqueue(cards[i]);
	}

	public void CleanTable()
	{
		onTable[0] = null;
		onTable[1] = null;
		onTable[2] = null;
	}

	public void ShuffleGraveyardToDeck()
	{
		ShuffleCardsToDeck(graveyardCards.ToList(), deckInstantiatedQueue);
		graveyardCards.Clear();
	}

	public void ResetCardPositions()
	{
		for (int i = 0; i < onTable.Length; i++)
			if (onTable[i] != null)
				onTable[i].GetComponent<IDragger>().ResetCard();
	}

	public void ResetDeck()
	{
		deckInstantiatedQueue.Clear();
		graveyardCards.Clear();

		foreach (GameObject obj in deckInstantiatedList)	Destroy(obj);
		for (int i = 0; i < levelArrayToDelete; i++)
			if(levelOfEachCard.Count > 0)	levelOfEachCard.RemoveAt(levelOfEachCard.Count - 1);

		levelArrayToDelete = 0;
		deckInstantiatedList.Clear();
	}

	public void AddCardToDeckList(GameObject card)
	{
		deckList.Add(card.name);
		levelOfEachCard.Add(card.GetComponent<CardStatus>().level);
		if (visualizer != null) visualizer.AddCard(card);
	}

	public void AddTemporaryCardToDeckList(GameObject card)
	{
		deckInstantiatedList.Add(card);
		graveyardCards.Enqueue(card);
		levelOfEachCard.Add(card.GetComponent<CardStatus>().level);
		levelArrayToDelete++;
	}

	public IEnumerator moveCardTo(RectTransform cardTransform, Vector3 toPosition, float duration)
	{ 
		if (GameManager.Instance.battleManager.applyingPlayerStatusEffects)
			yield break;

		float counter = 0;
		Vector3 startPos = cardTransform.position;

		GameManager.Instance.battleManager.applyingPlayerStatusEffects = true;

		while (counter < duration)
		{
			counter += Time.deltaTime;
			cardTransform.position = Vector3.Lerp(startPos, toPosition, counter / duration);
			yield return null;
		}

		GameManager.Instance.battleManager.applyingPlayerStatusEffects = false;
	}

	public void SwitchCards(int card1Index, int card2Index) // Troca as cartas mas nao as posicoes
	{
		GameObject card1 = onTable[card1Index];
		GameObject card2 = onTable[card2Index];

		CardStatus card1Status = card1.GetComponent<CardStatus>();
		CardStatus card2Status = card2.GetComponent<CardStatus>();

		// altera os slotIsOn das duas cartas entre si
		int card1Slot = card1Status.slotIsOn;
		card1Status.slotIsOn = card2Status.slotIsOn;
		card2Status.slotIsOn = card1Slot;

		// altera os modificadores
		CardModifier card1Modifier = card1Status.actualModifier;
		card1Status.actualModifier = card2Status.actualModifier;
		card2Status.actualModifier = card1Modifier;

		// troca carta em onTable no deck
		onTable[card1Index] = card2;
		onTable[card2Index] = card1;
	}

	public IEnumerator SwitchUpCards()
	{
		// Sorteia duas trocas binarias: 1 com 2 e 2 com 3
		// Isso desloca as cartas para as posicoes: 1 -> 3, 2 -> 1, 3 -> 2.
		List<int> choices = new List<int>();
		choices.Add(0);
		choices.Add(1);
		choices.Add(2);

		int card1Index = choices[Random.Range(0, (choices.Count - 1))];
		choices.Remove(card1Index);
		int card2Index = choices[Random.Range(0, (choices.Count - 1))];
		choices.Remove(card2Index);
		int card3Index = choices[0];

		// Transforms das cartas
		RectTransform card1Transform = onTable[card1Index].GetComponent<RectTransform>();
		RectTransform card2Transform = onTable[card2Index].GetComponent<RectTransform>();
		RectTransform card3Transform = onTable[card3Index].GetComponent<RectTransform>();

		// Posicoes originais das cartas
		Vector3 card1Position = card1Transform.position;
		Vector3 card2Position = card2Transform.position;
		Vector3 card3Position = card3Transform.position;

		float durationOfMovement = 0.15f;

		// Move as cartas para o centro
		Instantiate(GameManager.Instance.battleUIManager.battleUIParticles.clumsinessParticles,
					Vector3.zero,
					GameManager.Instance.battleUIManager.battleUIParticles.particleSpawnPosition.rotation,
					GameManager.Instance.battleUIManager.battleUI.transform);

		yield return StartCoroutine(moveCardTo(card1Transform, Vector3.zero, durationOfMovement));
		yield return StartCoroutine(moveCardTo(card2Transform, Vector3.zero, durationOfMovement));
		yield return StartCoroutine(moveCardTo(card3Transform, Vector3.zero, durationOfMovement));

		// Move as cartas para as posicoes baguncadas
		yield return StartCoroutine(moveCardTo(card1Transform, card3Position, durationOfMovement));
		yield return StartCoroutine(moveCardTo(card2Transform, card1Position, durationOfMovement));
		yield return StartCoroutine(moveCardTo(card3Transform, card2Position, durationOfMovement));

		// Troca de verdade as cartas
		SwitchCards(card1Index, card2Index);
		SwitchCards(card2Index, card3Index);
	}
}
