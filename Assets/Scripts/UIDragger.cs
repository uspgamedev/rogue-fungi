/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Programa que cuida da utilizacao das cartas do jogador durante a batalha*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIDragger : MonoBehaviour
{

	[Header("Essential")]
	public int screenWidth;
	public float velocidade;
	public float lowerLimit;
	public float higherLimit;
	public float moveDuration;
	public float lerpDuration;
	public float moveAmount;
	public float resto;
	public float mouseX;
	public float pontoAprox;
	public float moveFunctionSpeed;
	public Button exitButton;

	private bool hasTouched = false;
	private Tween myTween = null;
	public ShopManager shopManagerScript;
	public GameObject DialoguePanel;

	void Awake()
	{
		mouseX = screenWidth;
	}

	public void Update()
	{
		if(DialoguePanel.activeSelf == true)	return;

        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved && !hasTouched)
		{
			mouseX += Input.GetTouch(0).deltaPosition.x * moveAmount * 0.07f;

			if (mouseX >= higherLimit) mouseX = higherLimit;
			else if (mouseX <= lowerLimit) mouseX = lowerLimit;

			if(mouseX <= higherLimit && mouseX >= lowerLimit)	transform.localPosition = new Vector3(mouseX, transform.localPosition.y, 0);

			exitButton.enabled = false;
		}
		else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended && !hasTouched)
			hasTouched = true;
		else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
		{

			if (myTween != null)
				myTween.Kill();

			mouseX = transform.localPosition.x;
		}
        else if(Input.touchCount == 0 && hasTouched) // && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
			if (Mathf.Sign(mouseX) == -1) {
				mouseX *= -1;
				resto = mouseX % screenWidth;
				mouseX *= -1;
				if(resto != 0)
				{
					if(resto <= screenWidth / 2.0f)   pontoAprox = mouseX + resto;
					else    pontoAprox = ((mouseX + resto) - screenWidth);

					myTween = transform.DOLocalMove(new Vector3(pontoAprox, transform.localPosition.y, 0), lerpDuration).SetEase(Ease.OutBack);
					mouseX = pontoAprox;
				}
			}
			else
			{
				resto = mouseX % screenWidth;
				if(resto != 0)
				{
					if(resto <= screenWidth / 2.0f)   pontoAprox = mouseX - resto;
					else    pontoAprox = ((mouseX - resto) + screenWidth);

					myTween = transform.DOLocalMove(new Vector3(pontoAprox, transform.localPosition.y, 0), lerpDuration).SetEase(Ease.OutBack);
					mouseX = pontoAprox;
				}
			}

			exitButton.enabled = true;
			hasTouched = false;
        }
	}

	public void Move(int positionX)
	{
		if (myTween != null)
			myTween.Kill();

		myTween = transform.DOLocalMove(new Vector3(positionX, transform.localPosition.y, 0), moveFunctionSpeed).SetEase(Ease.OutBack);
		mouseX = positionX;
	}
}

