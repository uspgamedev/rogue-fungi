﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAnimation : MonoBehaviour
{
	public float timer = 1.0f;

    void Start()
    {
		IEnumerator coroutine = SelfDestruct();
        StartCoroutine(coroutine);
    }

	IEnumerator SelfDestruct()
	{
		yield return new WaitForSeconds(timer);
		Destroy(gameObject);
	}
}
