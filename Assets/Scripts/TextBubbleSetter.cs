﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextBubbleSetter : MonoBehaviour
{
	public SpriteRenderer backgroundSprite;
	public RectTransform backgroundRectTransform;
	public TextMeshPro bubbleText;
	public float pivotToSubtract;
	public float lineSize;

	public void Start()
	{
		pivotToSubtract = backgroundRectTransform.pivot.y;
		lineSize = backgroundSprite.size.y;
	}


	public void SetText(string t)
	{
		bubbleText.text = t;
	}
}
