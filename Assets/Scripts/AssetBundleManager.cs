﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

// Script from Unity Manual https://docs.unity3d.com/540/Documentation/Manual/keepingtrackofloadedassetbundles.html
// Modified
static public class AssetBundleManager
{
	static private Dictionary<string, AssetBundleRef> dictAssetBundleRefs;

	static AssetBundleManager()
	{
		dictAssetBundleRefs = new Dictionary<string, AssetBundleRef>();
	}

	private class AssetBundleRef
	{
		public AssetBundle assetBundle = null;
		public string fileName;
		public AssetBundleRef(string newFileName)
		{
			fileName = newFileName;
			assetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, newFileName));
		}
	};

	public static AssetBundle getAssetBundle(string fileName)
	{
		AssetBundleRef abRef;
		if (dictAssetBundleRefs.TryGetValue(fileName, out abRef))
			return abRef.assetBundle;
		else
			return null;
	}

	public static AssetBundle setAssetBundle(string fileName)
	{
		AssetBundleRef abRef = new AssetBundleRef(fileName);
		if (dictAssetBundleRefs.ContainsKey(fileName))
			dictAssetBundleRefs[fileName] = abRef;
		else
			dictAssetBundleRefs.Add(fileName, abRef);

		return abRef.assetBundle;
	}

	public static void Unload(string fileName, bool allObjects)
	{
		AssetBundleRef abRef;
		if (dictAssetBundleRefs.TryGetValue(fileName, out abRef))
		{
			abRef.assetBundle.Unload(allObjects);
			abRef.assetBundle = null;
			dictAssetBundleRefs.Remove(fileName);
		}
	}
}
