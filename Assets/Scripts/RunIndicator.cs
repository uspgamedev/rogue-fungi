﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunIndicator : MonoBehaviour
{
	public Transform runCounters;

    void Start()
    {
		int run = 0;
		if(GameManager.Instance.persistentRunData.runData.currentRun >= runCounters.childCount)
			run = runCounters.childCount - 1;
		else
			run = GameManager.Instance.persistentRunData.runData.currentRun;

		for (int counter = 0; counter <= run; counter++)
			runCounters.GetChild(counter).gameObject.SetActive(true);
    }
}
