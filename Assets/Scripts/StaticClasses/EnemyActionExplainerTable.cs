using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;
using UnityEngine.Localization.Tables;

public static class EnemyActionExplainerTable
{
	public static string[] tableEntries;

	public static bool IsInTable(string actionName)
	{
		for (int i = 0; i < tableEntries.Length; i++)
			if (actionName == tableEntries[i])
				return true;

		return false;
	}
}
