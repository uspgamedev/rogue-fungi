using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;

public enum CauldronBuffTypes { CHEST_RATE_2_STARS, REWARD_RATE_2_STARS, CHEST_RATE_3_STARS, REWARD_RATE_3_STARS, AFTER_BATTLE_RECOVERY, REST_RESTORE_RATE, FIRST_TURN_ACTION, ACTION_POINTS, SLOT_CHANGE_COST, MAX_HEALTH_POINTS}

[System.Serializable]
public class CauldronBuffClass
{

	[Header("Description")]
	public string name;
	public CauldronBuffTypes type;
	public DynamicLocaleText[] localizedText;
	
	[Header("Levels")]
	public int[] prices;
	public int[] buffValues;
	public int buffUnlockAmount;
}

[CreateAssetMenu(fileName = "New CauldronPack", menuName = "CauldronPack")]
public class CauldronBuffsPack : ScriptableObject
{
	public CauldronBuffClass[] cauldronBuffs;
}