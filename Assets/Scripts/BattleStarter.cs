﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStarter : MonoBehaviour
{
	public BattleManager battleManagerScript;
	void Awake()
	{
		battleManagerScript.SetupBattleReferences();
        SaveData dataToLoad = GameManager.Instance.loadGameManager.GetDataToLoad();

		if (GameManager.Instance.IsDoingNewBattle())
			battleManagerScript.StartBattle();
		else
			battleManagerScript.LoadBattle(GameManager.Instance.loadGameManager.GetDataToLoad());

		StartCoroutine(battleManagerScript.StartPlayerTurn(false));
	}

	public void PassTurn()
	{
		battleManagerScript.PassPlayerTurn();
	}
}
