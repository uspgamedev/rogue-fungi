﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyUnitData
{
	[Header("Saving Settings")]
	public string savingDialogue;

	[Header("Original Settings")]
	public int originalHealth;

	[Header("Actual Settings")]
	public int actualHealth;
	public int actualProtection;
	public int actualPoison; 
	public int actualSpecialProtection;
	public int gainedStrength = 0;

	[Header("Skills & Powers")]
	public int actualAttack1;
	public int actualAttack2;
	public int actualAttack3;
	public int actualPoisonPower;
	public int actualProtectionPower;
	
	[Header("Actions")]
	public int currentStage = 0;

	[Header("Others")]
	public float scale;

	[Header("Enemy Stage Based")]
	public int[] attack1PerStage;
	public int[] attack2PerStage;
	public int[] attack3PerStage;
	public int[] protectionPerStage;
	public int[] healthPerStage;
	public int[] specialProtectionPerStage;	
}

public class EnemyUnit : MonoBehaviour
{
	public EnemyUnitData enemyUnitData;

	[Header("Actions")]
	public EnemyActionPattern actionPattern;

	[Header("Color")]
	public Color normalColor;
	public Color damageColor;
	public Color poisonColor;
}
