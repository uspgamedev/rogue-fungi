﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PlayerBehaviourData
{
	public int lifeInitial;
	public int actionPointsInitial;
	public int defenseInitial;
	public int coinsRunStart;
	public int coins;
	public int xpRunStart;
	public int xp;
	public int lifeCurrent;
	public int actionPointsCurrent;
	public int defenseCurrent;
	public int poisonCurrent;
	public int tirednessCurrent;
	public int tirednessTurns;
	public int inflationCurrent;
	public int curseCurrent;
	public int clumsinessCurrent;
	public int destructionCurrent;
	public int[] slotBlockCurrent = new int[3];
}

public class PlayerBehaviour : MonoBehaviour
{
	public static PlayerBehaviour Instance { get; private set; }

	public PlayerBehaviourData playerBehaviourData;

	public BattleUIManager battleUIManagerScript;

	public Animator playerHealthBarAnimator;

	public Dictionary<CauldronBuffTypes, int[]> playerBuffs = new Dictionary<CauldronBuffTypes, int[]>()
	{
		{CauldronBuffTypes.AFTER_BATTLE_RECOVERY, new int[]{0,0}}, {CauldronBuffTypes.REST_RESTORE_RATE, new int[]{0,0}},
        {CauldronBuffTypes.CHEST_RATE_2_STARS, new int[]{0,0}}, {CauldronBuffTypes.REWARD_RATE_2_STARS, new int[]{0,0}},
		{CauldronBuffTypes.CHEST_RATE_3_STARS, new int[]{0,0}}, {CauldronBuffTypes.REWARD_RATE_3_STARS, new int[]{0,0}},
		{CauldronBuffTypes.FIRST_TURN_ACTION, new int[]{0,0}}, {CauldronBuffTypes.ACTION_POINTS, new int[]{0,0}}, 
		{CauldronBuffTypes.SLOT_CHANGE_COST, new int[]{0,0}}, {CauldronBuffTypes.MAX_HEALTH_POINTS, new int[]{0,0} }
	};

	public int totalBuffs;

	void Awake()
	{
		if (Instance != null && Instance != this)	Destroy(gameObject);
		else
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}

		playerBehaviourData.lifeCurrent = playerBehaviourData.lifeInitial;
		playerBehaviourData.actionPointsCurrent = playerBehaviourData.actionPointsInitial;
		playerBehaviourData.defenseCurrent = playerBehaviourData.defenseInitial;
	}

	public void TakePiercingDamage(int enemyAtk)
	{
		battleUIManagerScript.TakeDamage(enemyAtk);
		playerBehaviourData.lifeCurrent -= enemyAtk;
		battleUIManagerScript.ShowLifePoints();

		if (playerBehaviourData.lifeCurrent <= 0)
		{
			GameManager.Instance.battleManager.LostBattle();
			battleUIManagerScript.PlayerLose();
		}
	}

	public void TakeDamage(int enemyAtk)
	{
		int damageToTake = enemyAtk - playerBehaviourData.defenseCurrent;

		if (playerBehaviourData.defenseCurrent > 0)
		{
			playerBehaviourData.defenseCurrent -= enemyAtk;
			if (playerBehaviourData.defenseCurrent < 0)	playerBehaviourData.defenseCurrent = 0;
			battleUIManagerScript.ShowPlayerProtection();
		}

		if (damageToTake > 0)
		{
			battleUIManagerScript.TakeDamage(enemyAtk);
			playerBehaviourData.lifeCurrent -= damageToTake;
			battleUIManagerScript.ShowLifePoints();
		}

		if (playerBehaviourData.lifeCurrent <= 0)
		{
			playerBehaviourData.lifeCurrent = 0;
			battleUIManagerScript.ShowLifePoints();
			GameManager.Instance.battleManager.LostBattle();
			battleUIManagerScript.PlayerLose();
		}
	}

	public void TakePoison(int enemyPoison)
	{
		playerBehaviourData.poisonCurrent += enemyPoison;
	}

	public void TakePoisonDamage(int poison)
	{
		playerBehaviourData.lifeCurrent -= poison;
		battleUIManagerScript.ShowLifePoints();
		playerBehaviourData.poisonCurrent -= 1;
	}

	public void ResetPlayerStatus()
	{
		playerBehaviourData.defenseCurrent = 0;
		playerBehaviourData.poisonCurrent = 0;
		playerBehaviourData.tirednessCurrent = 0;
		playerBehaviourData.tirednessTurns = 0;
		playerBehaviourData.inflationCurrent = 0;
		playerBehaviourData.curseCurrent = 0;
		playerBehaviourData.clumsinessCurrent = 0;
	}

	// Chamada para curar a vida do jogador
	public void HealLife(int toHeal)
	{
		playerBehaviourData.lifeCurrent += toHeal;

		if (playerBehaviourData.lifeCurrent > playerBehaviourData.lifeInitial)
			playerBehaviourData.lifeCurrent = playerBehaviourData.lifeInitial;

		battleUIManagerScript.ShowLifePoints();
	}

	public bool IsDead()
	{
		return (playerBehaviourData.lifeCurrent <= 0);
	}
}
