﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class RewardSelector : MonoBehaviour, IDropHandler
{
	[Header("Setup")]
	public RewardManager rewardManager;
	public Canvas rewardCanvas;
	public GameObject[] buttons;
	public Image bookBackground;
	public RectTransform rewardPosition;
	public RectTransform cardInstantiatedPosition;
	public RectTransform cardInstantiatedScale;
	public Text descriptionText;
	public GameObject magicCircle;
	public Image bookDetectionArea;
	public AnimationClip openingAnimation;
	public Animator bookAnimator;
	public GameObject creationParticles;
	public GameObject chooseYourRewardTextUI;

	[Header("Current Reward")]
	public GameObject selectedReward;
	private GameObject cardInstantiated;

	private Vector2 endPosition = new Vector2(0.0f, 150.0f);
	private Vector3 endScale = new Vector3(0.2f, 0.2f, 1.0f);

	public void SetupBattleReward()
	{
		Logging.Log("SETUP");

		foreach (RewardObject rewardObject in rewardManager.battleRewardObjects)	rewardObject.gameObject.SetActive(true);

		descriptionText.enabled = false;
		bookBackground.raycastTarget = true;
	}

	public void SetupChestReward()
	{
		rewardManager.chestRewardObject.gameObject.SetActive(false);
		rewardManager.chestRewardSelector.gameObject.SetActive(false);

		descriptionText.enabled = false;
		bookBackground.raycastTarget = true;
	}

	public void SetRewardOnBook(PointerEventData pointerEventData)
	{
		if (pointerEventData == null)	return;	

		if (pointerEventData.lastPress != null)
		{
			if (pointerEventData.lastPress.gameObject.GetComponent<RewardObject>() == null) return;

			if (selectedReward != null && pointerEventData.lastPress.gameObject != selectedReward)	SnapBack(selectedReward);
			
			Logging.Log("Pointer nao nulo");
			selectedReward = pointerEventData.lastPress.gameObject;
			selectedReward.transform.DOMove(rewardPosition.position, 0.2f, true);
			SetSelectionElements(true);
		}
		else if (pointerEventData.pointerDrag != null)
		{
			if (pointerEventData.pointerDrag.gameObject.GetComponent<RewardObject>() == null) return;

			if (selectedReward != null && pointerEventData.pointerDrag.gameObject != selectedReward)	SnapBack(selectedReward);

			Logging.Log("Pointer nao nulo");
			selectedReward = pointerEventData.pointerDrag.gameObject;
			selectedReward.transform.DOMove(rewardPosition.position, 0.2f, true);
			SetSelectionElements(true);
		}
	}

	public void OnDrop(PointerEventData pointerEventData)
	{
		Logging.Log("OnDrop");
		SetRewardOnBook(pointerEventData);
	}

	public void SetSelectionElements(bool option)
	{
		foreach (GameObject button in buttons)	button.SetActive(option);

		descriptionText.enabled = option;
		if (selectedReward != null)
		{
			descriptionText.text = selectedReward.GetComponent<RewardObject>().CardDescription();
			selectedReward.GetComponent<RewardObject>().isSelected = option;
		}

		bookBackground.raycastTarget = (!option);
	}

	public void SnapBack(GameObject objectToSnapBack)
	{
		if (objectToSnapBack != null)	
			objectToSnapBack.transform.DOMove(objectToSnapBack.GetComponent<RewardObject>().originalPosition, 0.2f, true);
	}

	public void ReturnSelectedReward()
	{
		SnapBack(selectedReward);
		SetSelectionElements(false);
		selectedReward = null;
	}

	public void GetSelectedReward()
	{
		rewardManager.GetReward(selectedReward.GetComponent<RewardObject>().cardPrefab, selectedReward.GetComponent<RewardObject>().card.GetComponent<CardStatus>().level);
		SnapBack(selectedReward);
		foreach (RewardObject rewardObject in rewardManager.battleRewardObjects)	rewardObject.gameObject.SetActive(false);	

		rewardManager.chestRewardObject.gameObject.SetActive(false);

		cardInstantiated = selectedReward.GetComponent<RewardObject>().card;
									
		cardInstantiated.GetComponent<Animator>().enabled = false;
		cardInstantiated.GetComponent<IDragger>().enabled = false;
		cardInstantiated.GetComponent<IDragger>().enabled = false;
		cardInstantiated.GetComponent<RectTransform>().localScale = new Vector3(0.01f, 0.01f, 0);
		cardInstantiated.GetComponent<RectTransform>().position = transform.position;
		if(cardInstantiated.GetComponent<RectTransform>() != null)
			cardInstantiated.GetComponent<RectTransform>().DOAnchorPos(endPosition, 1.0f, true).SetEase(Ease.OutCubic).onComplete = CardRewardedFloatingUP;
		cardInstantiated.GetComponent<RectTransform>().DOScale(endScale, 0.01f).SetEase(Ease.OutCubic);
		creationParticles.SetActive(true);

        bookAnimator.SetBool("BookTransition", true);
		magicCircle.SetActive(false);
		descriptionText.gameObject.SetActive(false);
		chooseYourRewardTextUI.SetActive(false);
    }

	public void CardRewardedFloatingUP()
	{
		if(cardInstantiated != null && cardInstantiated.GetComponent<RectTransform>() != null)
			cardInstantiated.GetComponent<RectTransform>().DOAnchorPosY(endPosition.y + 20f, 3f).SetEase(Ease.InOutSine).onComplete = CardRewardedFloatingDOWN;
	}

    public void CardRewardedFloatingDOWN()
    {
		if(cardInstantiated != null && cardInstantiated.GetComponent<RectTransform>() != null)
        	cardInstantiated.GetComponent<RectTransform>().DOAnchorPosY(endPosition.y, 3f).SetEase(Ease.InOutSine).onComplete = CardRewardedFloatingUP;
    }

    public void OnDisable()
	{
		bookDetectionArea.enabled = false;
	}

	public void CleanBook()
	{
		if (cardInstantiated != null)	Destroy(cardInstantiated);
		foreach (GameObject button in buttons)	button.SetActive(false);

		magicCircle.SetActive(false);
		creationParticles.SetActive(false);
	}

	public void OnEnable()
	{
		StartCoroutine(ShowMagicCircle());
	}

	private IEnumerator ShowMagicCircle()
	{
		yield return new WaitForSeconds(openingAnimation.length);
		magicCircle.SetActive(true);
		bookDetectionArea.enabled = true;
	}
}
