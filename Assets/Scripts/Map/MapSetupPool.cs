using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New MapSetupPool", menuName = "MapSetupPool")]
public class MapSetupPool : ScriptableObject
{
	public MapSetup[] maps;
	public int[] indexes;
}