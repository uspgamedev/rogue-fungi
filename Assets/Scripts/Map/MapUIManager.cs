﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapUIManager : MonoBehaviour
{
	public GameObject mapManager;
	public MoneyDisplay moneyDisplayScript;
	public GameObject lineRendererPrefab;
	public Material lineRendererMaterial;
	public GameObject mapCursor;

	public void Start()
	{
		moneyDisplayScript.UpdateDisplay();
	}

}
