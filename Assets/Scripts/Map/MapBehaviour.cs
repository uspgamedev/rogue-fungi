﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class MapBehaviour : MonoBehaviour
{
	public MapUIManager mapUIManager;
	public GameObject mapUI;
	public GameObject mapCursorInstance = null;
	public MapSetup actualMap;
	public Button goBackButton;
	[HideInInspector] public GameObject[] placesOnMap;
	[HideInInspector] public GameObject[] lineRenderers;
	[HideInInspector] public int actualPlace;

	void Awake()
	{
		GameManager.Instance.mapBehaviourScript = this;
		GameUIManager.Instance.mapBehaviourScript = this;
	}

	void Start()
	{	

		if(GameManager.Instance.actualPlace != -1)
			actualPlace = GameManager.Instance.actualPlace;

		InitializeMap(MenuData.doNewGame);

		if (MapSceneTransition.mapSceneTransitionData.needToEnableNextPlace)
		{
			if (actualPlace == placesOnMap.Length - 1)
			{
				CleanMap();
				GameManager.Instance.state = GameState.SHOP_SCREEN;
			}
			else
			{
				EnableNextPlace();
				MapSceneTransition.mapSceneTransitionData.needToEnableNextPlace = false;
			}
		}

        if (!MapSceneTransition.mapSceneTransitionData.alreadyGotInformation || MenuData.doNewGame)
		{
			MapSceneTransition.mapSceneTransitionData.mapSize = placesOnMap.Length;
			MapSceneTransition.mapSceneTransitionData.placesOnMapEnabled = new bool[MapSceneTransition.mapSceneTransitionData.mapSize];
			MapSceneTransition.mapSceneTransitionData.visitedPlaces = new bool[MapSceneTransition.mapSceneTransitionData.mapSize];
			for (int i = 0; i < MapSceneTransition.mapSceneTransitionData.mapSize; i++)
				MapSceneTransition.mapSceneTransitionData.placesOnMapEnabled[i] = placesOnMap[i].GetComponent<Button>().interactable;

			MapSceneTransition.mapSceneTransitionData.alreadyGotInformation = true;
		}

		for (int i = 0; i < MapSceneTransition.mapSceneTransitionData.placesOnMapEnabled.Length; i++)
			Logging.Log("Lugares do mapa: " + i + " " + MapSceneTransition.mapSceneTransitionData.placesOnMapEnabled[i]);
		Logging.Log("Lugares do mapa: pronto");

		MenuData.doNewGame = false;
		GameManager.Instance.SaveGame();
	}

	public void InitializeMap(bool isNewMap)
	{
		if (isNewMap || GameManager.Instance.mapNumber == -1)
		{
			Logging.Log("Mapa: novo");
			GameManager.Instance.mapNumber = UnityEngine.Random.Range(0, GameManager.Instance.mapSetupPool.maps.Length);
			actualMap = GameManager.Instance.mapSetupPool.maps[GameManager.Instance.mapNumber];
			MapSceneTransition.mapSceneTransitionData.actualMap = GameManager.Instance.mapNumber;
			placesOnMap = new GameObject[actualMap.placeUnits.Length];
			lineRenderers = new GameObject[placesOnMap.Length];
			GameManager.Instance.actualMap = actualMap;
			GenerateNewMap(actualMap);
		}
		else
		{
			actualMap = GameManager.Instance.mapSetupPool.maps[GameManager.Instance.mapNumber];
			placesOnMap = new GameObject[actualMap.placeUnits.Length];
			lineRenderers = new GameObject[placesOnMap.Length];
			Logging.Log("Mapa: load");
			LoadMap(actualMap);
		}
	}

	public void GenerateNewMap(MapSetup setup)
	{
		LineRenderer lineRenderer;
		int count = 0;

		GameManager.Instance.battleSetupPool.indexes = new int[setup.placeUnits.Length];     
		GameManager.Instance.battleSetupPool.difficulties = new int[setup.placeUnits.Length];  
		foreach (Place place in setup.placeUnits)
		{
			placesOnMap[count] = Instantiate(place.placePrefab, mapUI.transform.position, mapUI.transform.rotation, mapUI.transform);
			placesOnMap[count].transform.localPosition = place.position;
			placesOnMap[count].GetComponent<PlaceBehaviour>().indexOnPlaceUnits = count;

			int random = UnityEngine.Random.Range(1, 100);
			if (random <= GameManager.Instance.persistentRunData.runData.level3EnemySpawnRate &&
				place.diff < 3)
			{
				placesOnMap[count].GetComponent<PlaceBehaviour>().battleSetup = GameManager.Instance.randomizeBattleSetup(count, 3, place);
				Debug.Log("Peguei monstro nível 3");
			}
			else if (random <= GameManager.Instance.persistentRunData.runData.level2EnemySpawnRate &&
					place.diff < 2)
			{
				placesOnMap[count].GetComponent<PlaceBehaviour>().battleSetup = GameManager.Instance.randomizeBattleSetup(count, 2, place);
				Debug.Log("Peguei monstro nível 2");
			}
			else if (random <= GameManager.Instance.persistentRunData.runData.level1EnemySpawnRate &&
				place.diff < 1)
			{
				placesOnMap[count].GetComponent<PlaceBehaviour>().battleSetup = GameManager.Instance.randomizeBattleSetup(count, 1, place);
				Debug.Log("Peguei monstro nível 1");
			}
			else
			{
				placesOnMap[count].GetComponent<PlaceBehaviour>().battleSetup = GameManager.Instance.randomizeBattleSetup(count, place.diff, place);
			}

			placesOnMap[count].transform.SetAsFirstSibling();
			if (place.startEnabled)
				placesOnMap[count].GetComponent<Button>().interactable = true;

			// Cria as linhas conectando os locais do mapa
			foreach (MapPath path in place.paths)
			{
				lineRenderers[count] = Instantiate(mapUIManager.lineRendererPrefab, mapUI.transform.position, mapUI.transform.rotation, mapUI.transform);
				lineRenderer = lineRenderers[count].GetComponent<LineRenderer>();

				// Pontos a desenhar
				Vector3[] points = new Vector3[path.intermediatePoints.Length + 2];
				points[0] = place.position;
				for (int i = 0; i < path.intermediatePoints.Length; i++)
					points[i + 1] = path.intermediatePoints[i];

				Place lastPlace = Array.Find<Place>(setup.placeUnits, placeToGo => placeToGo.name == path.destination);
				points[points.Length - 1] = lastPlace.position;

				lineRenderer.positionCount = points.Length;
				lineRenderer.SetPositions(points);
			}

			count++;
		}
		// Por fim, a linha partindo da casa para o primeiro local
		lineRenderers[count-1] = Instantiate(mapUIManager.lineRendererPrefab, mapUI.transform.position, mapUI.transform.rotation, mapUI.transform);
		lineRenderer = lineRenderers[count-1].GetComponent<LineRenderer>();

		// Pontos a desenhar
		Vector3[] initialPoints = new Vector3[setup.initialPath.intermediatePoints.Length + 2];
		initialPoints[0] = setup.startingPoint;
		for (int i = 0; i < setup.initialPath.intermediatePoints.Length; i++)
			initialPoints[i + 1] = setup.initialPath.intermediatePoints[i];

		Place firstPlace = Array.Find<Place>(setup.placeUnits, placeToGo => placeToGo.name == setup.initialPath.destination);
		initialPoints[initialPoints.Length - 1] = firstPlace.position;

		lineRenderer.positionCount = initialPoints.Length;
		lineRenderer.SetPositions(initialPoints);

		// Configurando o boss
		//BattleSetup currentBossSetup = GameManager.Instance.bossesBattleSetups[GameManager.Instance.currentBossIndex];
		//placesOnMap[count-1].GetComponent<PlaceBehaviour>().battleSetup = currentBossSetup;

		actualPlace = 0;
	}

	public void LoadMap(MapSetup setup)
	{
		LineRenderer lineRenderer;
		int count = 0;

		foreach (Place place in setup.placeUnits)
		{
			placesOnMap[count] = Instantiate(place.placePrefab, mapUI.transform.position, mapUI.transform.rotation, mapUI.transform);
			placesOnMap[count].transform.localPosition = place.position;
			placesOnMap[count].GetComponent<PlaceBehaviour>().indexOnPlaceUnits = count;
			placesOnMap[count].GetComponent<PlaceBehaviour>().battleSetup = GameManager.Instance.getBattleSetup(GameManager.Instance.battleSetupPool.difficulties[count], GameManager.Instance.battleSetupPool.indexes[count]);
			// placesOnMap[count].GetComponent<Button>().interactable = GameManager.Instance.currentData.isMapPlaceEnabled[count];
			placesOnMap[count].GetComponent<Button>().interactable = MapSceneTransition.mapSceneTransitionData.placesOnMapEnabled[count];

			if (MapSceneTransition.mapSceneTransitionData.visitedPlaces[count])
			{
				placesOnMap[count].GetComponent<PlaceBehaviour>().EnableVisitedSprite();
			}

			// Cria as linhas conectando os locais do mapa
			foreach (MapPath path in place.paths)
			{
				lineRenderers[count] = Instantiate(mapUIManager.lineRendererPrefab, mapUI.transform.position, mapUI.transform.rotation, mapUI.transform);
				lineRenderer = lineRenderers[count].GetComponent<LineRenderer>();
				// lineRenderer.material.SetFloat("Vector1_6DC4325C", 1.0f);

				// Pontos a desenhar
				Vector3[] points = new Vector3[path.intermediatePoints.Length + 2];
				points[0] = place.position; // partimos do local atual
				for (int i = 0; i < path.intermediatePoints.Length; i++)
				{ // passamos pelos pontos intermediarios
					points[i + 1] = path.intermediatePoints[i];
				}
				// e chegamos no destino
				Place lastPlace = Array.Find<Place>(setup.placeUnits, placeToGo => placeToGo.name == path.destination);
				points[points.Length - 1] = lastPlace.position;

				lineRenderer.positionCount = points.Length;
				lineRenderer.SetPositions(points);
			}

			count++;
		}
		// Por fim, a linha partindo da casa para o primeiro local
		lineRenderers[count - 1] = Instantiate(mapUIManager.lineRendererPrefab, mapUI.transform.position, mapUI.transform.rotation, mapUI.transform);
		lineRenderer = lineRenderers[count - 1].GetComponent<LineRenderer>();
		lineRenderer.positionCount = 2;
		lineRenderer.SetPositions(new Vector3[2] { setup.startingPoint, setup.placeUnits[0].position });
		Vector3 result = new Vector3(actualMap.placeUnits[actualPlace].position[0], actualMap.placeUnits[actualPlace].position[1], 0.0f);

		mapCursorInstance = Instantiate(mapUIManager.mapCursor, mapUI.transform.position, mapUI.transform.rotation, mapUI.transform);
		mapCursorInstance.transform.localPosition = result + (new Vector3(0.0f, 26.0f, 0.0f));
	}

	// torna possivel entrar na proxima sala do mapa
	public void EnableNextPlace(/*MapSetup thisMap, Place thisPlace*/)
	{

		for (int i = 0; i < actualMap.placeUnits.Length; i++) // percorre o vetor que contem todos os lugares que estao no mapa
		{
			placesOnMap[i].GetComponent<Button>().interactable = false;
			MapSceneTransition.mapSceneTransitionData.placesOnMapEnabled[i] = false;

			for (int j = 0; j < actualMap.placeUnits[actualPlace].paths.Length; j++) // percorre o vetor paths procurando os proximos lugares que o jogador podera entrar
			{

				if (actualMap.placeUnits[i].name == actualMap.placeUnits[actualPlace].paths[j].destination)
				{
					placesOnMap[i].GetComponent<Button>().interactable = true;
					MapSceneTransition.mapSceneTransitionData.placesOnMapEnabled[i] = true;
				}
			}
		}
	}

	public void CleanMap()
	{
		foreach (GameObject place in placesOnMap)	Destroy(place);
		foreach(GameObject lineRenderer in lineRenderers)	Destroy(lineRenderer);

		actualPlace = 0;
		placesOnMap = new GameObject[0];
	}

	public void BackToMenu()
	{
		GameUIManager.Instance.FromMapToInitialScreenScreen();
	}
}
