﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShakingOnEnable : MonoBehaviour
{

    [SerializeField] private Transform enemyTransform;
    [SerializeField] private float shakeDuration;
    [SerializeField] private float shakeStrength;

    public void OnEnable()
    {
        transform.DOShakePosition(shakeDuration, shakeStrength);
    }
}
