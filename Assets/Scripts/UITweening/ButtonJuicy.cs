﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class ButtonJuicy : MonoBehaviour
{
    private RectTransform thisRectTransform;
    private bool doJuicy = false;
    private float initialScaleY; // ATENCAO: Usa o scale do Y

    // Start is called before the first frame update
    void Start()
    {
        thisRectTransform = this.gameObject.GetComponent<RectTransform>();
    }

    // Efeito para o botao branco das UIs que deixa os botoes mais responsivos
    public void Juicy(float newScale)
    {
        if (doJuicy) return;

        thisRectTransform = this.gameObject.GetComponent<RectTransform>();

        initialScaleY = thisRectTransform.localScale.y;
        thisRectTransform.DOScale(newScale, 0.1f).SetEase(Ease.OutExpo).onComplete = UnJuicy;

        doJuicy = true;
    }

    // volta os efeitos ao normal
    public void UnJuicy()
    {
        thisRectTransform.DOScale(initialScaleY, 0.1f).SetEase(Ease.OutExpo);
        doJuicy = false;
    }

    public void DisapearCanvasGroup()
    {
        this.gameObject.GetComponent<CanvasGroup>().DOFade(0f, 1f);
    }
}
