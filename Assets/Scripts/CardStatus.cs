﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Contem os dados da carta quando sao copiadas do ScriptableObject para serem usadas no jogo*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class CardStatus : MonoBehaviour
{
	[Header("Setup")]
	public CardUnit card; // scriptable object que contem o prefab desta carta
	public KindOfCard kind; // tipo da carta
	public int actionPoints = 1; // quantos action points essa carta consome
	public int level;
	public int maxLevel;
	public List<CardsEffects> cardEffects = new List<CardsEffects>(); // todos os efeitos que essa carta contem
	public Sprite cardCenterSprite;
	public Sprite goldStar;
	public GameObject cardDescriptions;
	public string fileContent;
	public RectTransform cardTransform;

	[Header("References")]
	public Animator animator; // deve ser referenciado no inspector
	public List<SpriteRenderer> grayStars; // imagem da raridade da carta, muda conforme o nivel. Deve ser referenciado no inspector
	public Transform cardDescriptionTransform; // O transform com os textos de descricao
	public TextMeshPro costText; // texto do custo da carta
	public RuntimeAnimatorController defaultAnimation;
	public IDragger iDraggerScript;
	public BattleUIManager battleUIManagerScript;
	private List<string> originalCardDescription; // descricao original, para ser recuperada apos usar a carta

	[Header("Current information")]
	public int slotIsOn; // 0 = left, 1 = center, 2 = right
	public int indexOnDeck; // indice no deckList
	public CardModifier actualModifier = CardModifier.ORIGINAL; // modificador ativo no momento

	public bool[] hasUpgradeOnString; // Set on Editor


	public void Start()
	{
		BattleManager battleManager = GameManager.Instance.battleManager;
		if (battleManager != null)	battleUIManagerScript = battleManager.gameObject.GetComponent<BattleUIManager>();
		cardTransform = gameObject.GetComponent<RectTransform>();
	}

	public void Setup() // Configura todos os efeitos dessa carta
	{
		foreach (CardsEffects effect in cardEffects)	effect.Setup();
	}

	public void Awake()
	{
		originalCardDescription = new List<string>();
		for (int i = 0; i < cardDescriptionTransform.childCount; i++)
		{
			TextMeshPro effectText = cardDescriptionTransform.GetChild(i).GetComponent<TextMeshPro>();
			originalCardDescription.Add(effectText.text);
		}
	}

	public void ChangeCost(int newNumber, string newColor) // muda o texto de custo da carta
	{
		System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("<color=#[0-9A-F]{6}>([0-9]+)</color>");
		string replace = "<color=" + newColor + ">" + newNumber + "</color>";
		costText.text = regex.Replace(costText.text, replace);
	}
	
	public void DisableAndResetCard()
	{
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
		transform.localScale = new Vector3(0.17f, 0.17f, 0f);
		animator.runtimeAnimatorController = defaultAnimation as RuntimeAnimatorController;
		gameObject.SetActive(false);
	}

	public void UpgradeCardStars()
	{
		grayStars[0].sprite = goldStar;
		grayStars[0].gameObject.name = "Gold_Star" + level.ToString();
		grayStars.RemoveAt(0);
	}

}
