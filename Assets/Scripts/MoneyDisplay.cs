﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MoneyDisplay : MonoBehaviour
{
	public TextMeshPro textToUpdate;

	public void UpdateDisplay()
	{
		textToUpdate.text = PlayerBehaviour.Instance.playerBehaviourData.coins + "";
	}
}
