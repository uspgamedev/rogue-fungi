﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObjectsOnEnable : MonoBehaviour
{
	public GameObject[] objectsToEnable;
	public GameObject[] objectsToDisable;
	public bool alsoDisableSameObjects;

	public void OnEnable()
	{
		for (int i = 0; i < objectsToEnable.Length; i++) objectsToEnable[i].SetActive(true);
	}

	public void OnDisable()
	{
		if (alsoDisableSameObjects)
			for (int index = 0; index < objectsToEnable.Length; index++)
				if (objectsToEnable[index] != null)	objectsToEnable[index].SetActive(false);
		else
			for (int i = 0; i < objectsToDisable.Length; i++)
				objectsToDisable[i].SetActive(false);
	}
}
