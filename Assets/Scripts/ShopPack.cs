using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ShopPack", menuName = "ShopPack")]
public class ShopPack : ScriptableObject
{
	public string[] cards;
	public int[] price;
}