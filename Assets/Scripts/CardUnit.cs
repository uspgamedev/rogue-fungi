﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   ScriptableObject contendo os dados de uma determinada carta
   OBS: E' usado para armazenar variaveis que nao serao modificadas, e' como um template para criar algo novo*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
public class CardUnit : ScriptableObject
{
	[TextArea(2, 5)]
	public string description;

	public GameObject cardPrefab;
}
