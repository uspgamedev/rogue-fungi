﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
	void Start()
	{
		if (this.CompareTag("PlayerHealthBar"))
			PlayerBehaviour.Instance.playerHealthBarAnimator = this.GetComponent<Animator>();
	}
}
