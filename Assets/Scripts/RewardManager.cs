﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

[System.Serializable]
public struct SelectedCards{
	public string name;
	public int level;
	public int rewardIndex;
}

public class RewardManager : MonoBehaviour
{
	[Header("Essential scripts")]
	public SceneTransitionManager sceneTransitionManager;
	[Header("Map")]
	public MapBehaviour mapBehaviourScript;
	private MapSetup mapSetup;
	[Header("Battle Rewards")]
	public RewardSelector battleRewardSelector;
	public RewardObject[] battleRewardObjects;
	[Header("Chest Reward")]
	public RewardObject chestRewardObject;
	public RewardSelector chestRewardSelector;
	public Button chestButton;
	public GameObject chestText;
	[Header("Player Deck")]
	public DeckBehaviour playerDeck;
	[Header("Reward")]
	public RewardType currentType;
	public GameObject selectedRewardPrefab;
	public List<SelectedCards> selectedCards = new List<SelectedCards>();
	public int baseChanceFor2Stars;
	public int baseChanceFor3Stars;

	[Header("To choose the rewards")]
	public GameObject chestRewardUI;
	public GameObject battleRewardUI;

	public void Awake()
	{
		GameManager.Instance.rewardManager = this;
		playerDeck = GameObject.Find("PlayerDeck").GetComponent<DeckBehaviour>();
		sceneTransitionManager = GameObject.Find("LevelLoader").GetComponent<SceneTransitionManager>();
		
		if (RewardData.rewardType == 1)
		{
			battleRewardUI.SetActive(true);
			SetupBattleReward();
		}
		else
		{
			chestRewardUI.SetActive(true);
			chestRewardSelector.SetupChestReward();
		}

		GameManager.Instance.SaveGame();
	}

	public void SetupBattleReward()
	{
		if(GameManager.Instance.selectedCards.Count > 0)
		{
			selectedCards = GameManager.Instance.selectedCards;
			GameManager.Instance.selectedCards = new List<SelectedCards>();
			LoadBattleRewards();
		}
		else	ChooseBattleRewards();
	}

	public void SetupChestReward()
	{
		if(GameManager.Instance.selectedCards.Count > 0)
		{
			selectedCards = GameManager.Instance.selectedCards;
			GameManager.Instance.selectedCards = new List<SelectedCards>();
		}
		
		chestRewardSelector.SetupChestReward();
	}

	public void LoadBattleRewards()
	{
		currentType = RewardType.BATTLE;
		selectedCards = GameManager.Instance.currentData.savedRewardCards;
		GameObject card = null;
		
		for(int i = 0; i < battleRewardObjects.Length; i++)
		{
			card = Instantiate(GameManager.Instance.battleRewardCards[selectedCards[i].rewardIndex], battleRewardSelector.gameObject.transform);
			CardStatus status = card.GetComponent<CardStatus>();
			if(status.level < selectedCards[i].level)	UpgradeRewardCard(status, selectedCards[i].level);
			battleRewardObjects[i].SetupCardEssence(card, GameManager.Instance.battleRewardCards[selectedCards[i].rewardIndex]);
		}

		battleRewardSelector.SetupBattleReward();
	}
	
	public void ChooseBattleRewards()
	{
		currentType = RewardType.BATTLE;
		List<int> jaEscolhidos = new List<int>();

		if(GameManager.Instance.battleRewardCards.Length == 0)
			GameManager.Instance.battleRewardCards = GameManager.Instance.
					actualMap.placeUnits[GameManager.Instance.actualPlace].rewardSetup.cards;

		if (GameManager.Instance.battleRewardCards.Length == 0)
		{
			Logging.Log("Saiu fora da recompensa");
			return;
		}

		for(int i = 0; i < battleRewardObjects.Length; i++)
		{
			List<int> naoEscolhidos = Enumerable.Range(0, GameManager.Instance.battleRewardCards.Length - 1).Where(indice => !jaEscolhidos.Contains(indice)).ToList();
			
			int randomIndex = Random.Range(0, naoEscolhidos.Count);
			GameObject card = Instantiate(GameManager.Instance.battleRewardCards[naoEscolhidos[randomIndex]], battleRewardSelector.gameObject.transform);
			jaEscolhidos.Add(naoEscolhidos[randomIndex]);

			SelectedCards randomizedCard = new SelectedCards();
			randomizedCard.name = card.name;
			randomizedCard.rewardIndex = naoEscolhidos[randomIndex];
			randomizedCard.level = 0;

			CardStatus status = card.GetComponent<CardStatus>();

			RandomizeUpgradedReward(status, randomizedCard);
			selectedCards.Add(randomizedCard);

			battleRewardObjects[i].SetupCardEssence(card, GameManager.Instance.battleRewardCards[naoEscolhidos[randomIndex]]);
		}

		Logging.Log("Amount of Reward Cards: " + GameManager.Instance.battleRewardCards.Length);

		battleRewardSelector.SetupBattleReward();
	}

	public void OpenChest()
	{
		chestButton.GetComponent<Button>().enabled = false;
		GameObject card = null;

		if(selectedCards.Count <= 0 )
		{
			int randomIndex = Random.Range(0, GameManager.Instance.battleRewardCards.Length);
			card = Instantiate(GameManager.Instance.battleRewardCards[randomIndex], chestRewardSelector.gameObject.transform);
			
			SelectedCards randomizedCard = new SelectedCards();
			randomizedCard.rewardIndex = randomIndex;
			randomizedCard.name =  card.name;
			randomizedCard.level = 0;

			CardStatus status = card.GetComponent<CardStatus>();
			RandomizeUpgradedReward(status, randomizedCard);

			selectedCards.Add(randomizedCard);
			chestRewardObject.SetupCardEssence(card, GameManager.Instance.battleRewardCards[randomIndex]);
		}
		else
		{
			chestRewardObject.SetupCardEssence(card, GameManager.Instance.battleRewardCards[selectedCards[0].rewardIndex]);
			card = GameManager.Instance.battleRewardCards[selectedCards[0].rewardIndex];
		} 

		StartCoroutine(WaitToSpawnChestReward());
		StartCoroutine(WaitForBook());
	}

	public void RandomizeUpgradedReward(CardStatus status, SelectedCards randomizedCard)
	{
		int chance = Random.Range(0, 100);
		int chanceFor3Stars = baseChanceFor3Stars;
		int chanceFor2Stars = baseChanceFor3Stars + baseChanceFor2Stars;

		if(RewardData.rewardType == 1)
		{
			chanceFor3Stars += PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.REWARD_RATE_3_STARS][0];
			chanceFor2Stars += PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.REWARD_RATE_2_STARS][0];
		}
		else
		{
			chanceFor3Stars += PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.CHEST_RATE_3_STARS][0];
			chanceFor2Stars += PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.CHEST_RATE_2_STARS][0];
		}

		if (chance <= chanceFor3Stars && status.level + 1 <= status.maxLevel)
		{
			UpgradeRewardCard(status, 2);
			randomizedCard.level = 2;
		}
		else if (chance <= chanceFor2Stars && status.level + 1 <= status.maxLevel)
		{
			UpgradeRewardCard(status, 1);
			randomizedCard.level = 1;
		}
	}

	public static void UpgradeRewardCard(CardStatus cardStatus, int times)
	{
		for(int i = 0; i < times; i++)
		{
			Logging.Log("Upgrade realizado");
			if(cardStatus.level < cardStatus.maxLevel)
			{
				cardStatus.level++;
				cardStatus.UpgradeCardStars();
				foreach (CardsEffects effect in cardStatus.cardEffects)
					effect.Upgrade(cardStatus.level, false);
			}
		}
	}

	public IEnumerator WaitForBook()
	{
		yield return new WaitForSeconds(0.7f);
        chestRewardSelector.gameObject.SetActive(true);
    }

	public IEnumerator WaitToSpawnChestReward()
	{
        yield return new WaitForSeconds(0.25f);
        chestRewardObject.gameObject.SetActive(true);
		chestRewardObject.transform.DOLocalMoveY(155f, 0.4f).
			OnComplete(chestRewardObject.FinishedAnimation);
		chestButton.gameObject.GetComponent<Animator>().SetBool("ToOpenChest", true);
		chestText.gameObject.SetActive(false);
    }

	public void ResetChestPlace()
	{
		chestRewardSelector.CleanBook();

		chestRewardObject.gameObject.SetActive(false);
		chestRewardSelector.gameObject.SetActive(false);
		chestButton.gameObject.SetActive(true);
		chestText.gameObject.SetActive(true);

		chestRewardSelector.SnapBack(chestRewardSelector.selectedReward);
		chestRewardSelector.selectedReward = null;
	}

	public void ResetRewardPlace()
	{
		battleRewardSelector.CleanBook();
		battleRewardSelector.SnapBack(battleRewardSelector.selectedReward);
		battleRewardSelector.selectedReward = null;
	}

	public void GetReward(GameObject cardPrefab, int level)
	{
		int index = playerDeck.deckList.Count;
		cardPrefab.GetComponent<CardStatus>().indexOnDeck = index;
		playerDeck.AddCardToDeckList(cardPrefab);
	}

	public void GetBackToMap()
	{
		DOTween.KillAll();
		sceneTransitionManager.ChangeBackToMapScene();
	}
}
